# README #

-> This is a sample Selenium Project to Automate S2P Buying Process using SAP Ariba for University of Sydney
### What is this repository for? ###

* Quick summary
Running an automation test to purchase items on Unibuy using Java with Selenium Bindings. TestNG Framework has been used along with Serenity BDD.

### How do I get set up? ###

* Summary of set up
This project needs Java Runtime Environment 1.8 setup to run.

* Configuration

Browser Driver Selection
------------------------
Browser can be configured in Serenity.properties file

Base URL
---------
-> Base URL of the SUT can be added in the HomePage.java file. It is defined in the annotation @DefaultURL

To clone the repository
------------------------
Git has to be installed to clone/download the repository.
From command prompt, run "git clone https://kiranmudigonda@bitbucket.org/kiranmudigonda/bdd-task-serenity-java-maven-new-archetype.git"


How to run tests
-----------------

Open command prompt and navigate to the project home directory.
From the project home directory, run "SerenityTestRun <browser name>" 
Available parameters for the browser name are: chrome, firefox

After tests are run, go to \target\site\serenity and open index.html to find the test report

### Who do I talk to? ###

* Repo owner - Kiran Mudigonda (kiran.mudigonda@sydney.edu.au)