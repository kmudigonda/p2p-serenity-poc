package com.bddtask.serenity.steps.serenity;

import org.openqa.selenium.Dimension;

import com.bddtask.serenity.ariba.pages.AdvancePaymentsPage;
import com.bddtask.serenity.ariba.pages.CatalogPage;
import com.bddtask.serenity.ariba.pages.CheckOutPage;
import com.bddtask.serenity.ariba.pages.Common;
import com.bddtask.serenity.ariba.pages.HomePage;
import com.bddtask.serenity.ariba.pages.InvoicingPage;
import com.bddtask.serenity.ariba.pages.LineItemDetailPage;
import com.bddtask.serenity.ariba.pages.NonCatalogPage;
import com.bddtask.serenity.ariba.pages.ProcurementPage;
import com.bddtask.serenity.ariba.pages.PurchaseRequestPage;
import com.bddtask.serenity.ariba.pages.ReceivePage;
import com.bddtask.serenity.ariba.pages.SearchPRPage;
import com.bddtask.serenity.ariba.pages.ShoppingBasket;
import com.bddtask.serenity.ariba.pages.SplitAccountingPage;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class S2PSteps extends ScenarioSteps {

	HomePage hp;
	ProcurementPage procurement;
	InvoicingPage invoicing;
	Common cmn;
	CatalogPage catalog;
	ShoppingBasket shopping;
	CheckOutPage checkout;
	LineItemDetailPage lineItems;
	PurchaseRequestPage prpage;
	ReceivePage receive;
	SearchPRPage searchPRPage;
	NonCatalogPage nonCatalogPage;
	SplitAccountingPage splitPage;
	AdvancePaymentsPage apmtPage;

	@Step
	public void openHomePage() {
		cmn.open();

		if (System.getProperty("chrome.switches").equals("--headless")) {
			getDriver().manage().window().setSize(new Dimension(1920, 1080));
		} else {
			getDriver().manage().window().maximize();
		}
		hp.checkForLogin();
	}

	@Step
	public void goToProcurement() {
		cmn.goToProcurement();
	}

	@Step
	public void goToInvoicing() {
		cmn.goToInvoicing();
	}

	@Step
	public void clickCreateButton() {
		cmn.clickCreateBtn();
	}

	@Step
	public void goToHome() {
		cmn.goToHome();
		hp.homePageLoadCheck();
	}

	@Step
	public void goToCatalog() {
		cmn.goToCatalog();
	}

	public void selectRecentlyViewedItemTile() {
		catalog.clickonRecentlyViewedItemTile();
	}

	@Step
	public void searchForItem(String search) throws InterruptedException {
		catalog.enterSearchItem(search);
	}

	@Step
	public void enterSearchQty(String searchQty) throws InterruptedException {
		catalog.enterSearchQty(searchQty);
	}

	@Step
	public void proceedToCheckout() {
		catalog.proceedToCheckOut();
	}

	public void inputShipTo(String ShipTo) {
		checkout.inputShipTo(ShipTo);
	}

	@Step
	public void addPRTitle() {
		checkout.addPRTitle(cmn.timeStampAdder("Automation Test Create PR"));
	}

	@Step
	public void createRequisition() {
		procurement.clickRequisitionLink();
	}

	@Step
	public void createInvoice() {
		invoicing.clickInvoiceLink();
	}

	@Step
	public void searchBySupplier() {
		catalog.hoverOverSupplier();
		cmn.threadSleepWait(5);
	}

	@Step
	public void inputDeliveryInfo(String deliveryInfo) {
		cmn.threadSleepWait(2);
		checkout.inputDeliveryInfo(deliveryInfo);
	}

	@Step
	public void editLineDetails() {
		checkout.editLineDetails();
	}

	@Step
	public void submitPROK() {
		lineItems.clickOk();
	}

	@Step
	public void submitPR() {
		checkout.submitPR();
	}

	@Step
	public void checkPRNumber() {
		procurement.confirmPRNumber();
	}

	@Step
	public void iOpenMyPR() {
		procurement.clickPR("mydocs");
	}

	@Step
	public void checkPRStatus(String string1, String string2, String string3) {
		prpage.checkPRStatus(string1, string2, string3);
	}
	
	@Step
	public void checkPRStatus(String string1) {
		prpage.checkPRStatus(string1);
	}

	@Step
	public void checkPRStatus(String string, String string2) {
		prpage.checkPRStatus(string, string2);
	}

	@Step
	public void clickReceiveBtn() {
		prpage.clickReceive();
	}

	@Step
	public void acceptAll() {
		receive.acceptAll();
	}

	@Step
	public void submitReceipt() {
		receive.receiptSubmit();
	}

	@Step
	public void poDispatchWait(int durationMins) {
		cmn.waitForPSEvent(durationMins);
	}

	@Step
	public void clickSearchIcon() {
		cmn.clickSearchIconBtn();
	}

	@Step
	public void clickRequisition() {
		cmn.clickRequisitionSearch();
	}

	@Step
	public void clickPOSearch() {
		cmn.clickPOSearch();
	}

	@Step
	public void searchForPR(String prID) {
		searchPRPage.searchPRID(prID);
	}

	@Step
	public void searchForPO(String poID) {
		searchPRPage.searchPOID(poID);
	}

	@Step
	public void openSearchedItem(String prID) {
		searchPRPage.openSearchedItem(prID);
	}

	@Step
	public void deletePRBtn() {
		prpage.clickDeleteBtn();
	}

	@Step
	public void checkResultsNum(String searchNum) {
		searchPRPage.verifySearchResultsNum(searchNum);
	}

	@Step
	public void copyPRBtn() {
		prpage.clickCopyBtn();
	}

	@Step
	public void inputReasonForPurchase(String reasonForPurchase) {
		checkout.inputReasonForPurchase(reasonForPurchase);
	}

	@Step
	public void archibusSelectStep(String archibusOptionSelection) {
		checkout.archibusSelect(archibusOptionSelection);
	}

	@Step
	public void clickNonCat() {
		catalog.clickNonCatBtn();
	}

	@Step
	public void commodityCodeSelectStep(String commodityCode) {
		nonCatalogPage.enterCommodityCode(commodityCode);
	}

	@Step
	public void inputItemDesc() {
		nonCatalogPage.enterItemDescription("Test Item");
	}

	@Step
	public void noCatQtyUomStep(String qty, String uom) {
		nonCatalogPage.enterQuantity(qty);
		nonCatalogPage.enterUOM(uom);

	}

	@Step
	public void noCatPriceCurr(String itemPrice, String currency) {
		nonCatalogPage.enterPrice(itemPrice);
		nonCatalogPage.selectCurrency(currency);
	}

	@Step
	public void nonCatSupplier(String supplier) {
		nonCatalogPage.selectSupplier(supplier);
	}

	@Step
	public void selectNonPOInvoice() {
		invoicing.selectNonPOInvoice();
	}

	@Step
	public void searchForSupplier(String supplierSelect) {
		invoicing.selectSupplierDropDown();
		invoicing.searchAndSelectSupplier(supplierSelect);
	}

	@Step
	public void fillDateAndInvoiceNumber() {
		invoicing.fillInvoiceNumber();
	}

	@Step
	public void addCatalogItemToInvoice() {
		invoicing.clickAddItems();
		invoicing.selectAddCatalogItem();
	}

	@Step
	public void addToCart() {
		catalog.addToCart();
	}

	@Step
	public void clickAccountingInfoLink1() {
		invoicing.clickAccountingInfoLink1();
	}

	@Step
	public void clickAccountingInfoLink2() {
		invoicing.clickAccountingInfoLink2();
	}

	@Step
	public void inputInvoiceAccountingInformation(String location, String ccCode, String rcCode, String pcCode) {
		invoicing.inputLocation(location);
		invoicing.inputccCode(ccCode);
		invoicing.inputrcCode(rcCode);
		invoicing.inputpcCode(pcCode);
	}

	@Step
	public void inputAccountingInformation(String location, String ccCode, String rcCode, String pcCode)
			throws InterruptedException {
		lineItems.inputLocation(location);
		lineItems.inputCC(ccCode);
		lineItems.inputRC(rcCode);
		lineItems.inputPC(pcCode);
	}

	@Step
	public void validateExit() {
		invoicing.clickValidateExit();
	}

	@Step
	public void selectTaxCode(String taxCode, String taxType) {
		invoicing.clickAddItemDetails();
		invoicing.selectTaxCode(taxCode, taxType);
	}

	@Step
	public void addShippingItem() {
		invoicing.addShippingItem();
	}

	@Step
	public void invoiceSubmit() {
		invoicing.submitInvoice();
	}

	@Step
	public void clickRefreshBtn() {
		cmn.clickRefreshBtn();
	}

	@Step
	public void clickSubmittedInvoice() {
		invoicing.clickSubmittedInvoice();
	}

	@Step
	public void clickFirstItemChk() {
		invoicing.clickFirstItemChk();
	}

	@Step
	public void clickSecondItemChk() {
		invoicing.clickSecondItemChk();
	}

	@Step
	public void addShippingCharges(String shippingCharges) {
		invoicing.addShippingCharges(shippingCharges);
	}

	@Step
	public void i_open_searched_pr() {
		searchPRPage.clickFirstSearchResult();
	}

	@Step
	public void addNCItemToCart() {
		nonCatalogPage.addNCItemToCart();
	}

	@Step
	public void showApprovalFlow() {
		checkout.showApprovalFlow();
	}

	@Step
	public void clickSplitAccounting() {
		lineItems.clickSplitAccounting();
	}

	@Step
	public void selectSplitBy(String splitBySelection) {
		splitPage.clickSplitDropdown();
		splitPage.selectSplit(splitBySelection);
	}

	@Step
	public void addSplit1AccountingValues(String location1, String CC1, String RC1, String PC1, String SPLIT1) {
		splitPage.addSplit1AccountingValues(location1, CC1, RC1, PC1, SPLIT1);
	}

	@Step
	public void addSplit2AccountingValues(String location2, String CC2, String RC2, String PC2, String SPLIT2) {
		splitPage.addSplit2AccountingValues(location2, CC2, RC2, PC2, SPLIT2);
	}

	@Step
	public void splitAccountingOk() {
		splitPage.clickOk();
		cmn.threadSleepWait(3);
	}

	@Step
	public void clickOnTodoPR() {
		procurement.clickPR("todo");
	}

	@Step
	public void clickApprovePR() {
		procurement.approvePR();
		cmn.threadSleepWait(4);
	}

	@Step
	public void checkIfApproverPresent(String approverGroup) {
		checkout.checkIfApproverPresent(approverGroup);
	}

	@Step
	public void clickBackBtn() {
		cmn.clickBackBtn();
	}

	@Step
	public void clickCreateAdvPmtBtn() {
		apmtPage.clickCreateApmt();
	}

	@Step
	public void selectHdrLvl() {
		apmtPage.selectHdrLvl();
	}
	
	@Step
	public void inputApmtTitle(String apmtTitleString) {
		apmtPage.inputTitle(apmtTitleString);	
	}

	@Step
	public void inputApmtAmount(String apmtAmountString) {
		apmtPage.inputAmount(apmtAmountString);
	}

	@Step
	public void selectTodaysDate() {
		apmtPage.selectTodaysDate();
	}

	@Step
	public void clickSubmit() {
		cmn.clickSubmit();
	}
	
	@Step
	public void changeApprover(String approverName, String typeOfApprover, String deleteApprover) {
		checkout.changeApprover(approverName,typeOfApprover, deleteApprover);
	}

	@Step
	public void selectPOInvoice() {
		invoicing.selectPOInvoice();
	}

	@Step
	public void inputInvoicePONumber(String poNumber) {
		invoicing.inputPONumber(poNumber);
	}

	@Step
	public void selectHdrLvlChkBox() {
		invoicing.selectHdrLvlChkBox();
	}

	@Step
	public void capturePONumber() {
		prpage.capturePONumber();
	}

	@Step
	public void clickInvoiceSearch() {
		cmn.clickInvoiceSearch();		
	}

	@Step
	public void inputInvoiceNumber(String invoiceNumber) {
		cmn.searchInvoiceByNumber(invoiceNumber);
	}

	@Step
	public void clickOnTab(String tabName) {
		cmn.clickOnTab(tabName);
	}

	@Step
	public void checkInvoiceStatus(String invoiceExpectedStatus) {
		invoicing.checkStatus(invoiceExpectedStatus);
	}

	@Step
	public void clickOnIRLink(String invoiceNumber) {
		invoicing.clickOnIRLink(invoiceNumber);		
	}

	@Step
	public void changeIRApprover(String irApproverName) {
		invoicing.changeIRApprover(irApproverName);
	}

	@Step
	public void deleteApprover() {
		invoicing.deleteApprover();
	}
}