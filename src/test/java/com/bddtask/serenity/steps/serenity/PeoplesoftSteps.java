package com.bddtask.serenity.steps.serenity;

import com.bddtask.serenity.peoplesoft.pages.BasePage;

import net.thucydides.core.annotations.Step;

public class PeoplesoftSteps {

	BasePage basePage;
	
	@Step
	public void loginToPS() {
		basePage.loginToPS();
	}

	@Step
	public void clickOnNavigatorBtn() {
		basePage.clickOnNavigatorBtn();
	}

	@Step
	public void goToNavigator() {
		basePage.goToNavigator();
	}

	@Step
	public void clickOnPurchasing() {
		basePage.goToPurchaseOrders();
	}
	
	@Step
	public void openPoId(String PO_ID) {
		basePage.openPoId(PO_ID);
		
	}

	@Step
	public void verifyPoStatus() {
		basePage.verifyPoStatus();
		
	}

	@Step
	public void goToReviewPO() {
		basePage.goToReviewPO();
		
	}

	@Step
	public void goToDispatch() {
		basePage.goToDispatch();
		
	}

	@Step
	public void setRunControlId(String runControlId) {
		basePage.setRunControlId(runControlId);
		
	}

}
