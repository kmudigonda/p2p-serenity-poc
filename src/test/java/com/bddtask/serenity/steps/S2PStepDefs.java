package com.bddtask.serenity.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

import java.util.List;
import java.util.Map;

import com.bddtask.serenity.ariba.pages.Common;
import com.bddtask.serenity.steps.serenity.S2PSteps;

import net.thucydides.core.annotations.Steps;

public class S2PStepDefs {

	Common cmn;

	@Steps
	S2PSteps searchSteps;

	@Given("^I open Ariba as a requester$")
	public void i_open_ariba_as_a_requester() throws Throwable {
		searchSteps.openHomePage();
	}

	@When("^I navigate to Unibuy$")
	public void i_navigate_to_unibuy() {
		cmn.open();
	}

	@When("^I go to Home page$")
	public void i_go_to_home_page() throws Throwable {
		searchSteps.goToHome();
	}

	@When("^I go to Procurement page$")
	public void i_go_to_procurement_page() {
		searchSteps.goToProcurement();
	}

	@When("^I go to Invoicing page$")
	public void i_go_to_invoicing_page() {
		searchSteps.goToInvoicing();
	}

	@When("^I go to Catalog page$")
	public void i_go_to_catalog_page() throws Throwable {
		searchSteps.goToCatalog();
	}

	@When("^I select recently viewed item tile$")
	public void i_select_recently_viewed_item_tile() throws Throwable {
		searchSteps.selectRecentlyViewedItemTile();
	}

	@When("^I proceed to checkout$")
	public void i_proceed_to_checkout() throws Throwable {
		searchSteps.proceedToCheckout();
	}

	@When("^I click on create button$")
	public void i_click_on_create_button() throws Throwable {
		searchSteps.clickCreateButton();
	}

	@When("^I click on Requisition$")
	public void i_click_on_requisition() throws Throwable {
		searchSteps.createRequisition();
	}

	@When("^I click on Invoice$")
	public void i_click_on_invoice() {
		searchSteps.createInvoice();
	}

	@When("^I select Non-PO Invoice$")
	public void i_select_non_po_invoice(){
		searchSteps.selectNonPOInvoice();
	}

	@When("^I select PO Invoice$")
	public void i_select_po_invoice() {
		searchSteps.selectPOInvoice();
	}
	
	@When("^I search by Supplier$")
	public void i_search_by_supplier() throws Throwable {
		searchSteps.searchBySupplier();
	}

	@When("^I input Invoice Date and Invoice Number$")
	public void i_input_date_and_invoice_number() {
		searchSteps.fillDateAndInvoiceNumber();
	}

	@When("^I Add Catalog Item$")
	public void i_add_catalog_item() throws Throwable {
		searchSteps.addCatalogItemToInvoice();
	}

	@When("^I search for \"([^\"]*)\"$")
	public void i_search_for_black_whiteboard_markers(String searchItem) throws Throwable {
		searchSteps.searchForItem(searchItem);
	}

	@When("^I select Invoice Supplier as \"([^\"]*)\"$")
	public void i_select_supplier_(String supplierSelect) throws Throwable {
		searchSteps.searchForSupplier(supplierSelect);
	}

	@When("^I enter quantity as \"([^\"]*)\"$")
	public void i_enter_quantity_on_search_page(String Qty) throws Throwable {
		searchSteps.enterSearchQty(Qty);
	}

	@When("^I click add to cart on search results$")
	public void i_add_to_cart() {
		searchSteps.addToCart();
	}

	@When("^I click on Accounting Info link of the first item$")
	public void i_click_accounting_info1() {
		searchSteps.clickAccountingInfoLink1();
	}

	@When("^I click on Accounting Info link of the second item$")
	public void i_click_accounting_info2() {
		searchSteps.clickAccountingInfoLink2();
	}

	@When("^I enter accounting details for shipping item as \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_accounting_details_for_shipping(String location, String ccCode, String rcCode, String pcCode)
			throws InterruptedException {
		i_enter_location_cc_rc_and_pc_codes(location, ccCode, rcCode, pcCode);

	}

	@When("^I fill \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\" fields on Checkout page$")
	public void i_fill_checkout_page_fields(String ReasonForPurchase, String ShipTo, String DeliveryInfo)
			throws Throwable {
		searchSteps.addPRTitle();
		searchSteps.inputReasonForPurchase(ReasonForPurchase);
		searchSteps.inputShipTo(ShipTo);
		searchSteps.inputDeliveryInfo(DeliveryInfo);
	}

	@When("^I click on Actions followed by edit line details$")
	public void i_click_on_actions_followed_by_edit_line_details() throws InterruptedException {
		Thread.sleep(2000L);
		searchSteps.editLineDetails();
	}

	@When("^I enter \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_location_cc_rc_and_pc_codes(String location, String ccCode, String rcCode, String pcCode)
			throws InterruptedException {
		searchSteps.inputAccountingInformation(location, ccCode, rcCode, pcCode);
	}

	@When("^I enter Invoice \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
	public void i_enter_invoice_location_cc_rc_and_pc_codes(String location, String ccCode, String rcCode,
			String pcCode) throws InterruptedException {
		searchSteps.inputInvoiceAccountingInformation(location, ccCode, rcCode, pcCode);
	}

	@When("^I validate and exit$")
	public void i_click_validate_exit() {
		searchSteps.validateExit();
	}

	@When("^I click on \"([^\"]*)\" button$")
	public void i_click_refresh_btn(String btnName) {
		if (btnName.toLowerCase().equals("refresh")) {
			searchSteps.clickRefreshBtn();
		} else if (btnName.toLowerCase().equals("back")) {
			searchSteps.clickBackBtn();
		}

	}

	@When("^I click on \"([^\"]*)\" tab$")
	public void i_click_on_tab(String tabName) {
		searchSteps.clickOnTab(tabName);
	}
	
	@When("^I change IR Approver to \"([^\"]*)\"$")
	public void i_change_ir_approver(String irApproverName) {
		searchSteps.changeIRApprover(irApproverName);
	}
	
	@When("^I delete unwanted approver$")
	public void i_delete_unwanted_approver() {
		searchSteps.deleteApprover();
	}
	
	@When("^I click on the submitted invoice in to do list$")
	public void i_click_submitted_invoice() {
		searchSteps.clickSubmittedInvoice();
	}

	@When("^I select first item checkbox$")
	public void i_select_first_item_chk() {
		searchSteps.clickFirstItemChk();
	}

	@When("^I wait for \"([^\"]*)\" seconds")
	public void i_wait(int timeInSecs) {
		cmn.threadSleepWait(timeInSecs);
	}

	@When("^I input shipping charges as \"([^\"]*)\"")
	public void i_input_shipping_charges(String shippingCharges) {
		searchSteps.addShippingCharges(shippingCharges);
	}

	@When("^I select second item checkbox$")
	public void i_select_second_item_chk() {
		searchSteps.clickSecondItemChk();
	}

	@When("^I submit the invoice$")
	public void i_submit_invoice() {
		searchSteps.invoiceSubmit();
	}

	@When("^I click OK$")
	public void i_click_ok() {
		searchSteps.submitPROK();
	}

	@When("^I click OK in Split Accounting$")
	public void i_click_ok_split_accounting() {
		searchSteps.splitAccountingOk();
	}

	@When("^I click Submit Purchase Request$")
	public void i_click_submit_purchase_request() {
		searchSteps.submitPR();
	}

	@When("^I click on the PR in TO DO list$")
	public void i_click_on_pr_in_todo_list() {
		searchSteps.clickOnTodoPR();
	}

	@When("^I approve PR$")
	public void i_click_on_approve_btn() {
		searchSteps.clickApprovePR();
	}

	@When("^I click on Show Approval Flow$")
	public void show_approval_workflow() {
		searchSteps.showApprovalFlow();
	}

	@When("^my PR number should be visible$")
	public void my_pr_number_should_be_visible() {
		searchSteps.checkPRNumber();
	}

	@When("^I open my PR$")
	public void i_open_my_pr() {
		searchSteps.iOpenMyPR();
	}

	@When("^status of my PR should be \"([^\"]*)\", \"([^\"]*)\" or \"([^\"]*)\"$")
	public void status_of_pr_should_be_submitted_ordering_or_ordered(String status1, String status2, String status3) {
		searchSteps.checkPRStatus(status1, status2, status3);
	}

	@When("^I change the approver to \"([^\"]*)\" as \"([^\"]*)\" Approver, delete \"([^\"]*)\"$")
	public void i_change_the_approver_to(String approverName, String approverType, String deleteApprover) {
		searchSteps.changeApprover(approverName, approverType, deleteApprover);
	}
	
	@When("^status of my PR should be \"([^\"]*)\"$")
	public void status_of_pr_should_be_submitted(String status1) {
		searchSteps.checkPRStatus(status1);
	}
	
	@When("^status of my PR should be Ordering or Ordered$")
	public void status_of_pr_should_be_ordering_or_ordered() {
		searchSteps.checkPRStatus("Ordering", "Ordered");
	}

	@When("^I wait for \"([^\"]*)\" minutes for PO is dispatched$")
	public void i_wait_for_po_dispatch(String duration) {
		int parsedInt = Integer.parseInt(duration);
		searchSteps.poDispatchWait(parsedInt);
	}

	@When("^\"([^\"]*)\" should be part of the approvers$")
	public void check_for_approver_group(String approverGroup) {
		searchSteps.checkIfApproverPresent(approverGroup);
	}

	@When("^if PR Status is Ordered I click on Receive$")
	public void i_click_on_receive() {
		searchSteps.clickReceiveBtn();
	}

	@When("^I click on the Search icon and select \"([^\"]*)\"$")
	public void i_click_on_search_icon(String searchItem) {
		searchSteps.clickSearchIcon();
		String searchItemType = searchItem.toLowerCase();
		if (searchItemType.equals("requisition")) {
			searchSteps.clickRequisition();
		} else if (searchItemType.equals("purchase order")) {
			searchSteps.clickPOSearch();
		} else if (searchItemType.equals("invoice")) {
			searchSteps.clickInvoiceSearch();
		} 
	}

	@When("^I input invoice number as \"([^\"]*)\"$")
	public void input_invoice_number(String invoiceNumberInput) {
		cmn.invoiceNumber = invoiceNumberInput;
		searchSteps.inputInvoiceNumber(cmn.invoiceNumber);
	}
	
	@When ("^I click on IR link$")
	public void clickOnIRLink() {
		searchSteps.clickOnIRLink(cmn.invoiceNumber);
	}
	
	@When("^status of the Invoice should be \"([^\"]*)\"$")
	public void status_of_the_invoice_should_be(String invoiceExpectedStatus) {
		searchSteps.checkInvoiceStatus(invoiceExpectedStatus);
	}
	
	@When("^I accept all$")
	public void i_accept_all() {
		searchSteps.acceptAll();
	}

	@When("^I Submit Receipt$")
	public void i_submit_receipt() {
		searchSteps.submitReceipt();
	}

	@When("^I search for \"([^\"]*)\" Number \"([^\"]*)\"$")
	public void i_search_pr_number(String searchItem, String searchItemID) {
		if (searchItem.toLowerCase().equals("pr")) {
			searchSteps.searchForPR(searchItemID);
		} else if (searchItem.toLowerCase().equals("po")) {
			searchSteps.searchForPO(searchItemID);
		}
	}

	@When("^I select tax code as \"([^\"]*)\"$")
	public void i_select_tax_code(String taxCode) {
		searchSteps.selectTaxCode(taxCode, "itemTax");
	}

	@When("^I input PO Number as \"([^\"]*)\"$")
	public void i_input_po_number(String poNumber) {
		searchSteps.inputInvoicePONumber(poNumber);
	}
	
	@When("^I input already created PO Number")
	public void i_input_already_created_po_number() {
		searchSteps.inputInvoicePONumber(cmn.PONumber);
	}
	
	@When("^I capture the PO Number$")
	public void i_capture_po_number() {
		searchSteps.capturePONumber();
	}
	
	
	@When("^I select shipping tax code as \"([^\"]*)\"$")
	public void i_select_shipping_tax_code(String shippingTaxCode) {
		searchSteps.selectTaxCode(shippingTaxCode, "shippingTax");
	}

	@When("^I open the searched \"([^\"]*)\"$")
	public void i_open_the_searched(String documentID) {
		searchSteps.openSearchedItem(documentID);
	}

	@When("^I input Advance Payment Title$")
	public void i_input_title_as() {
		searchSteps.inputApmtTitle(cmn.timeStampAdder("Advance Payment"));
	}

	@When("^I submit$")
	public void i_submit() {
		searchSteps.clickSubmit();
	}
	
	@When("^I input Amount as \"([^\"]*)\"$")
	public void i_input_amount_as(String apmtAmountString) {
		searchSteps.inputApmtAmount(apmtAmountString);
	}
	
	@When("^I click on Create Advance Payment$")
	public void click_create_advance_payment() {
		searchSteps.clickCreateAdvPmtBtn();
	}
	
	@When("^I select today's date$")
	public void i_select_todays_date() {
		searchSteps.selectTodaysDate();
	}
	
	@When("^select Header Level$")
	public void select_header_level() {
		searchSteps.selectHdrLvl();
	}

	@When("^I search for the created \"([^\"]*)\" number and open it$")
	public void i_search_for_the_created_pr_by_number(String searchItemType) {
		if (searchItemType.toLowerCase().equals("pr")) {
			i_search_pr_number("pr", Common.PRNumber);
		}
		searchSteps.i_open_searched_pr();
	}

	@When("^I click on Delete button$")
	public void i_click_on_delete_btn() {
		searchSteps.deletePRBtn();
	}

	@When("^I add shipping item$")
	public void i_add_shipping_item() {
		searchSteps.addShippingItem();
	}

	@When("^I click on Copy button$")
	public void i_click_on_copy_btn() {
		searchSteps.copyPRBtn();
	}

	@When("^\"([^\"]*)\" results should be found$")
	public void results_should_be_found(String searchNum) {
		searchSteps.checkResultsNum(searchNum);
	}

	@When("^I check Archibus option as \"([^\"]*)\"$")
	public void archibusSelection(String archibusOptionSelection) {
		searchSteps.archibusSelectStep(archibusOptionSelection);
	}

	@When("^I click on Non-Catalog Item Button$")
	public void nonCatBtn() {
		searchSteps.clickNonCat();
	}

	@When("^I input Commodity Code as \"([^\"]*)\"$")
	public void commodityCode(String commodityCode) {
		searchSteps.commodityCodeSelectStep(commodityCode);
	}

	@When("^I input Item Description$")
	public void inputItemDescription() {
		searchSteps.inputItemDesc();
	}

	@When("^I input Quantity as \"([^\"]*)\" and Unit of Measure as \"([^\"]*)\"$")
	public void nonCatQtyandUom(String qty, String uom) {
		searchSteps.noCatQtyUomStep(qty, uom);
	}

	@When("^I input item price as \"([^\"]*)\" and Currency as \"([^\"]*)\"$")
	public void nonCatPriceCurrency(String itemPrice, String currency) {
		searchSteps.noCatPriceCurr(itemPrice, currency);
	}

	@When("^I select Supplier as \"([^\"]*)\"$")
	public void selectSupplier(String supplier) {
		searchSteps.nonCatSupplier(supplier);
	}

	@When("^I login to Ariba as AP Team Member$")
	public void loginAPTeam() {
		searchSteps.openHomePage();
	}

	@When("^I add non catalog item to cart$")
	public void addNCItemToCart() {
		searchSteps.addNCItemToCart();
	}

	@When("^I click on Split Accounting$")
	public void clickSplitAccounting() {
		searchSteps.clickSplitAccounting();
	}

	@When("^I select Split by \"([^\"]*)\"$")
	public void selectSplitBy(String splitBySelection) {
		searchSteps.selectSplitBy(splitBySelection);
	}

	@When("^I enter first split accounting combination as$")
	public void firstSplitValues(DataTable table) {
		List<Map<String, String>> data = table.asMaps(String.class, String.class);
		for (Map<String, String> fieldValue : data) {
			searchSteps.addSplit1AccountingValues(fieldValue.get("Location1"), fieldValue.get("CC1"),
					fieldValue.get("RC1"), fieldValue.get("PC1"), fieldValue.get("SPLIT1"));
		}
	}

	@When("^I enter second split accounting combination as$")
	public void secondSplitValues(DataTable table) {
		List<Map<String, String>> data = table.asMaps(String.class, String.class);
		for (Map<String, String> fieldValue : data) {
			searchSteps.addSplit2AccountingValues(fieldValue.get("Location2"), fieldValue.get("CC2"),
					fieldValue.get("RC2"), fieldValue.get("PC2"), fieldValue.get("SPLIT2"));
		}
	}
	
	@When("^I select header level checkbox for items$")
	public void select_header_level_checkbox_for_items() {
		searchSteps.selectHdrLvlChkBox();
	}
	
	@When("^I add \"([^\"]*)\" as \"([^\"]*)\" Approver, delete \"([^\"]*)\"$")
	public void changeApprover(String approverName, String typeOfApprover, String deleteApprover) {
		searchSteps.changeApprover(approverName, typeOfApprover, deleteApprover);
	}
}