package com.bddtask.serenity.steps;

import com.bddtask.serenity.ariba.pages.Common;
import com.bddtask.serenity.steps.serenity.PeoplesoftSteps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class PeoplesoftStepDefs {

	Common cmn;

	@Steps
	PeoplesoftSteps PSSteps;

	@Given("^I login to Peoplesoft$")
	public void i_login_to_peoplesoft() {
		PSSteps.loginToPS();
	}
	
	@Given("^I go to Navigator$")
	public void i_go_to_navigator() {
		PSSteps.clickOnNavigatorBtn();
		PSSteps.goToNavigator();
	}
	
	@And("^I go to Review Purchase Order Information$")
	public void i_go_to_review_po_information() {
		PSSteps.clickOnPurchasing();
		PSSteps.goToReviewPO();
	}
	
	@And("^I open PO id \"([^\"]*)\"$")
	public void i_go_to_PO_id(String PO_ID) {
		PSSteps.openPoId(PO_ID);
	}
	
	@Then("^The status of PO should be Approved or Dispatched$")
	public void verify_PO_Status() {
		PSSteps.verifyPoStatus();
	}
	
	@And("^I go to Dispatch POs$")
	public void go_to_DispatchPOs() {
		PSSteps.clickOnPurchasing();
		PSSteps.goToDispatch();
	}
	
	@And("^I use run control id as \"([^\"]*)\"$")
	public void set_run_control_id(String runControlId) {
		PSSteps.setRunControlId(runControlId);
		cmn.threadSleepWait(5);
	}

}