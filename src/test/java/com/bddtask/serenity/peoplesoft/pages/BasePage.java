package com.bddtask.serenity.peoplesoft.pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.bddtask.serenity.ariba.pages.Common;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class BasePage extends PageObject {

	Common cmn;

	@FindBy(id = "userid")
	private WebElementFacade useridTextBox;

	@FindBy(id = "pwd")
	private WebElementFacade pwdTextBox;

	@FindBy(css = "input.ps-button")
	private WebElementFacade psLoginBtn;

	@FindBy(id = "PT_NAVBAR")
	private WebElementFacade psNavBarBtn;

	@FindBy(xpath = "//ul[@class='ps_grid-body']/li[3]/div[2]")
	private WebElementFacade navigatorBtn;
	
	@FindBy(xpath = "//a[contains(text(),'Purchasing')]")
	private WebElementFacade purchasingLink;

	@FindBy(xpath = "//a[contains(text(),'Review PO Information')]")
	private WebElementFacade reviewPOInfoLink;
	
	@FindBy(xpath = "//a[contains(text(),'Purchase Orders')]")
	private WebElementFacade purchaseOrders;
	
	@FindBy(id = "PO_SRCH2_PO_ID")	
	private WebElementFacade poSearchInputBox;

	@FindBy(className = "PSPUSHBUTTONTBSEARCH")
	private WebElementFacade searchButton;
	
	@FindBy(id = "\\#ICCancel")
	private WebElementFacade cancelButton;
	
	@FindBy(id = "\\#ICSave")
	private WebElementFacade okButton;
	
	@FindBy(id = "PSXLATITEM_XLATSHORTNAME")
	private WebElementFacade poStatus;
	
	@FindBy(linkText = "Dispatch POs")
	private WebElementFacade dispatchPOs;
	
	@FindBy(id = "RUN_CNTL_PUR_RUN_CNTL_ID")
	private WebElementFacade runControlId;
	
	@FindBy(id = "PRCSRQSTDLG_WRK_LOADPRCSRQSTDLGPB")
	private WebElementFacade runButton;
	
	@FindBy(id = "PRCSRQSTDLG_WRK_SELECT_FLAG\\$0")
	private WebElementFacade poDispatchCheckBox;
	
	public String getSerenityPropertiesValues(String keyString) {
		EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
		String peoplesoftBaseURL = variables.getProperty(keyString);
		return peoplesoftBaseURL;
	}

	public void loginToPS() {
		getDriver().get(getSerenityPropertiesValues("peoplesoft.base.url"));
		getDriver().manage().window().maximize();
		String userName = getSerenityPropertiesValues("peoplesoft.login.username");
		String password = getSerenityPropertiesValues("peoplesoft.login.password");
		useridTextBox.sendKeys(userName);
		pwdTextBox.sendKeys(password);
		psLoginBtn.click();

	}

	public void clickOnNavigatorBtn() {
		psNavBarBtn.click();
	}

	public void goToNavigator() {

		getDriver().switchTo().frame("psNavBarIFrame");
		navigatorBtn.click();
	}

	public void goToPurchaseOrders() {
		purchasingLink.click();
		purchaseOrders.click();
	}
	
	public void goToReviewPO() {
		reviewPOInfoLink.click();
		purchaseOrders.click();				
	}

	public void openPoId(String PO_ID) {
		getDriver().switchTo().frame("ptifrmtgtframe");
		poSearchInputBox.sendKeys(PO_ID);
		searchButton.click();		
	}

	public void verifyPoStatus() {
		String poStatusText = poStatus.getText().toLowerCase();
		Assert.assertTrue((poStatusText.equals("dispatched" ))||(poStatusText.equals( "approved")));
		
	}

	public void goToDispatch() {
		dispatchPOs.click();
		
	}

	public void setRunControlId(String runControlId) {
		getDriver().switchTo().frame("ptifrmtgtframe");
		this.runControlId.sendKeys(runControlId);
		searchButton.click();
		runButton.click();
		List<WebElement> frames =  getDriver().findElements(By.cssSelector("[id^='ptModFrame_']"));
		System.out.println(frames.get(0).getCssValue("id"));
		getDriver().switchTo().frame(frames.get(0).getCssValue("id"));
		poDispatchCheckBox.click();
		cancelButton.click();
	}
	
	
}