package com.bddtask.serenity.ariba.pages;
import org.junit.Assert;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("https://s1-eu.ariba.com/Buyer/Main?realm=universityofsydney-usyd-T")

public class HomePage extends PageObject {
	
	@FindBy(xpath = "//input[@name='UserName']")
	private WebElementFacade userName;
	
	@FindBy(xpath = "//input[@name='Password']")
	private WebElementFacade password;
	
	@FindBy(xpath = "//span[@class='submit'][contains(.,'Sign in')]")
	private WebElementFacade signIn;
	
	@FindBy(xpath = "//div[@class='a-tile-chart-title'][contains(.,'To Do')]")
	private WebElementFacade toDoTileChart;
	
	@FindBy(xpath = "//div[@class='a-tile-title'][contains(.,'My Requisitions')]")
	private WebElementFacade myRequisitionsTile;
	
	@FindBy(xpath = "//div[@class='a-tile-title'][contains(.,'My Receipts')]")
	private WebElementFacade myReceiptsTile;
	
	@FindBy(xpath = "//td[@class='leg-font-wght-b portletTitle'][contains(.,'My Documents')]")
	private WebElementFacade myDocumentsTable;
	
	@FindBy(xpath = "//td[@class='leg-font-wght-b portletTitle'][contains(.,'To Do')]")
	private WebElementFacade toDoTable;
	
	Common cmn = new Common();
	
	public void homePageLoadCheck() {
		Assert.assertTrue(cmn.myDocumentsTable.isVisible());
		Assert.assertTrue(cmn.toDoTable.isVisible());
		Assert.assertTrue(toDoTileChart.isVisible());
		Assert.assertTrue(myRequisitionsTile.isVisible());
		Assert.assertTrue(myReceiptsTile.isVisible());
	}

	public void checkForLogin() {
		if (getDriver().getTitle().contains("Sign In")) {
			userName.sendKeys("");
			password.sendKeys("");
			signIn.click();
		}
		
	}
}