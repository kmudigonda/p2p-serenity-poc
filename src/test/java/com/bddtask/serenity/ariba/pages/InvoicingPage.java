
package com.bddtask.serenity.ariba.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.findby.By;

public class InvoicingPage extends PageObject {
	
	Common cmn;
	CheckOutPage checkout;
	public static String autoGenInvNo;
	
	@FindBy(xpath= "//div[@id='BPR_Body_Inner']/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr/td/div/div/table/tbody/tr/td/div/div/div/div/div/div/div/table/tbody/tr[1]/td/table/tbody/tr[2]/td/a")
	private WebElementFacade createInvoiceLink;
	
	@FindBy(xpath= "//label[contains(.,'Type')]/following::label[1]/div/label")
	private WebElementFacade nonPORadioBtn;
	
	@FindBy(xpath="//label[contains(.,'Type')]/following::label[4]")
	private WebElementFacade PORadioBtn;
	
	@FindBy(xpath= "//label[contains(.,'Supplier:')]/following::div[2]/div/span/div/div[2]/a/div/div")
	private WebElementFacade nonPOSupplierSelectDropDown;
	
	@FindBy(xpath = "//div[@id='BPR_Body_Inner']/div/div/a/div")
	private WebElementFacade nonPOSupplierSearchMore;
	
	@FindBy(xpath = "//input[@maxlength='50']")
	private WebElementFacade InvoiceNumberInput;
	
	@FindBy(xpath = "//label[contains(.,'Date')]/following::input[1]")
	private WebElementFacade InvoiceDateInput;
	
	@FindBy(xpath = "//span[contains(.,'Add Item Details')]")
	private WebElementFacade addItemDetails;
	
	@FindBy(xpath = "(//a[contains(.,'Taxes')])[6]")
	private WebElementFacade addTaxes;
	
//	@FindBy(xpath = "//a[@role='menuitem'][contains(.,'Taxes')]")
	@FindBy(xpath = "(//div[@id='addItemDetail']/a)[1]")
	private WebElementFacade addShippingTaxes;
	
	@FindBy(xpath = "(//label[contains(@bh,'RDO')])[5]")
	private WebElementFacade taxCodeRadioBtn;
	
	@FindBy(xpath = "(//button[contains(.,'Select')])[1]")
	private WebElementFacade taxCodeExcludeSelect;
	
	@FindBy(xpath = "(//button[contains(.,'Select')])[2]")
	private WebElementFacade taxCodegst10acqcapSelect;
	
	@FindBy(xpath = "(//button[contains(.,'Select')])[3]")
	private WebElementFacade taxCodegst10acqotherSelect;
	
	@FindBy(xpath = "(//button[contains(.,'Select')])[4]")
	private WebElementFacade taxCodegstzeroacqcapSelect;
	
	@FindBy(xpath = "(//button[contains(.,'Select')])[5]")
	private WebElementFacade taxCodegstzeroacqotherSelect;
	
	@FindBy(xpath = "(//button[contains(.,'Select')])[6]")
	private WebElementFacade taxCodegstzeroacqprivuseSelect;
	
	@FindBy(css = ".w-btn-primary:not([title='Submit'])")
	private WebElementFacade addTaxBtn;
	
	@FindBy(xpath = "(//span[contains(.,'Add Item')])[2]")
	private WebElementFacade addItem;
	
	@FindBy(xpath = "//div[@id='addItem_MovedCopy']/div[@id='addItem']/a[1]")
	private WebElementFacade addNonCatalogItem;
	
	@FindBy(xpath = "//div[@id='addItem_MovedCopy']/div[@id='addItem']/a[2]")
	private WebElementFacade addCatalogItem;
	
	@FindBy(xpath = "//div[@id='addItem_MovedCopy']/div[@id='addItem']/a[3]")
	private WebElementFacade addShippingItem;
	
	@FindBy(xpath = "//td[contains(.,'Shipping Cost')]/following::input[1]")
	private WebElementFacade shippingChargesInput;
	
	@FindBy(xpath = "(//div[contains(.,'(1)')])[17]")
	private WebElementFacade firstItemAccountingInfoLink;
	
	@FindBy(xpath = "(//div[contains(.,'(1)')])[19]")
	private WebElementFacade secondItemAccountingInfoLink;
	
	@FindBy(xpath = "(//div[contains(.,'(1)')])[21]")
	private WebElementFacade thirdItemAccountingInfoLink;
	
	@FindBy(xpath = "(//div[@title='Select from the list'])[3]")
	private WebElementFacade invoiceLocationDropDown;
	
	@FindBy(xpath = "(//div[@title='Select from the list'])[4]")
	private WebElementFacade invoiceCCDropDown;
	
	@FindBy(xpath = "(//div[@title='Select from the list'])[5]")
	private WebElementFacade invoiceRCDropDown;
	
	@FindBy(xpath = "(//div[@title='Select from the list'])[6]")
	private WebElementFacade invoicePCDropDown;
	
	@FindBy(xpath = "(//div[@class='w-chSearchLink'][contains(.,'Search more')])[last]")
	private WebElementFacade invoiceMenuSearchMore;
	
	@FindBy(xpath = "(//div[@class='w-chSearchLink'][contains(.,'Search more')])[7]")
	private WebElementFacade invoiceLocationSearchMore;
	
	@FindBy(xpath = "(//div[@class='w-chSearchLink'][contains(.,'Search more')])[3]")
	private WebElementFacade invoiceCCSearchMore;
	
	@FindBy(xpath = "(//div[@class='w-chSearchLink'][contains(.,'Search more')])[4]")
	private WebElementFacade invoiceRCSearchMore;
	
	@FindBy(xpath = "(//div[@class='w-chSearchLink'][contains(.,'Search more')])[5]")
	private WebElementFacade invoicePCSearchMore;
	
	@FindBy(xpath = "(//span[contains(.,'Validate and Exit')])[2]")
	private WebElementFacade validateAndExitBtn;
	
	@FindBy(xpath = "(//label[contains(@bh,'CHK')])[9]")
	private WebElementFacade firstItemChkBox;
	
	@FindBy(xpath = "(//label[contains(@bh,'CHK')])[11]")
	private WebElementFacade secondItemChkBox;
	
	@FindBy(xpath = "(//label[contains(@bh,'CHK')])[13]")
	private WebElementFacade thirdItemChkBox;
	
	@FindBy(xpath = "(//span[contains(.,'Submit')])[2]")
	private WebElementFacade invoiceSubmitBtn;
	
	@FindBy(id="_b8kfj")
	private WebElementFacade invoicePONumber;
	
	@FindBy(xpath = "(//label[@bh='CHK'])[10]")
	private WebElementFacade hdrLvlChkbox;
	
	@FindBy(className = "a-arp-approvable-state")
	private WebElementFacade invoiceStatus;
	
	@FindBy(xpath = "(//span[contains(@title,'Delete')])[3]")
	private WebElementFacade deleteApprover;
	
//	@FindBy(xpath = "//span[contains(.,'OK')]")
	@FindBy(xpath = "//button[contains(@class,'w-btn w-btn-primary')]")
	private WebElementFacade changeApproverOkBtn;
	
	@FindBy(xpath = "(//div[contains(@class,'w-apv-dropdown-icon')])[2]")
	private WebElementFacade actionsDropDownArrow;
	
	
	public void clickInvoiceLink() {
		createInvoiceLink.click();
	}
	
	public void selectPOInvoice(){
		cmn.threadSleepWait(2);
		PORadioBtn.click();
	}
	
	public void selectNonPOInvoice(){
		cmn.threadSleepWait(2);
		nonPORadioBtn.click();
	}
	
	public void selectSupplierDropDown() {
		cmn.threadSleepWait(1);
		nonPOSupplierSelectDropDown.click();
	}

	public void searchAndSelectSupplier(String supplierSelect) {
		nonPOSupplierSearchMore.click();
		cmn.selectMenuWithoutSearchMore(supplierSelect);
	}
	
	public void fillInvoiceNumber() {
		String todaysDate = cmn.invoiceDateGenerator();
		autoGenInvNo = cmn.timeStampAdder("inv-");
		cmn.threadSleepWait(1);
		InvoiceDateInput.sendKeys(todaysDate);
		cmn.threadSleepWait(1);
		InvoiceNumberInput.sendKeys(autoGenInvNo);
	}

	public void clickAddItems() {
		addItem.click();
	}

	public void selectAddCatalogItem() {
		addCatalogItem.click();
		cmn.threadSleepWait(3);
	}

	public void clickAccountingInfoLink1() {
		firstItemAccountingInfoLink.click();
	}
	
	public void clickAccountingInfoLink2() {
		secondItemAccountingInfoLink.click();
	}
	
	public void inputLocation(String location){
		cmn.threadSleepWait(2);
		cmn.clickWhenReady(invoiceLocationDropDown, 2);
		cmn.threadSleepWait(5);
		accountingComboSelect(location);
//		invoiceLocationSearchMore.click();
//		cmn.selectMenuWithoutSearchMore(location);
		}

	public void inputccCode(String ccCode) {
		cmn.threadSleepWait(1);
		cmn.clickWhenReady(invoiceCCDropDown, 2);
//		invoiceCCSearchMore.click();
		accountingComboSelect(ccCode);
	}
	
	public void inputrcCode(String rcCode) {
		cmn.threadSleepWait(1);
		cmn.clickWhenReady(invoiceRCDropDown, 2);
//		invoiceRCSearchMore.click();
		accountingComboSelect(rcCode);
	}
	
	public void inputpcCode(String pcCode) {
		cmn.threadSleepWait(1);
		cmn.clickWhenReady(invoicePCDropDown, 2);
//		invoicePCSearchMore.click();
		accountingComboSelect(pcCode);
	}
	
	private void accountingComboSelect(String accountingCode) {

	}
	
	public void clickValidateExit() {
		cmn.threadSleepWait(1);
		validateAndExitBtn.click();
	}

	public void clickAddItemDetails() {
//		firstItemChkBox.click();
		addItemDetails.click();		
	}

	public void selectTaxCode(String taxCode, String taxType) {
		if(taxType.equalsIgnoreCase("itemtax"))
		{
			addTaxes.click();
			taxCodeRadioBtn.click();
		} else if (taxType.equalsIgnoreCase("shippingtax")) {
			addShippingTaxes.click();
			taxCodeRadioBtn.click();
			cmn.threadSleepWait(1);
		}
		switch(taxCode) {
			case "GST-10-ACQ-OTHER":
				taxCodegst10acqotherSelect.click();
				break;
			case "GST-10-ACQ-CAP":
				taxCodegst10acqcapSelect.click();
				break;
			case "EXCLUDE":
				taxCodeExcludeSelect.click();
				break;
			case "GST-ZERO-ACQ-OTHER":
				taxCodegstzeroacqotherSelect.click();
				break;
			case "GST-ZERO-ACQ-CAP":
				taxCodegstzeroacqcapSelect.click();
				break;
			case "GST-ZERO-ACQ-PRIVUSE":
				taxCodegstzeroacqprivuseSelect.click();
				break;
		}
//		waitFor(addTaxBtn);
		cmn.threadSleepWait(3);
		addTaxBtn.click();
	}
	
	public void addShippingItem() {
		cmn.threadSleepWait(1);
		clickAddItems();
		addShippingItem.click();
	}
	
	public void submitInvoice() {
		cmn.threadSleepWait(2);
		invoiceSubmitBtn.click();
	}

	public void clickSubmittedInvoice() {
		WebElement todoInvoice = getDriver().findElement(By.partialLinkText(autoGenInvNo));
		todoInvoice.click();
		cmn.threadSleepWait(15);
	}

	public void clickFirstItemChk() {
		firstItemChkBox.click();
		cmn.threadSleepWait(3);
	}
	
	public void clickSecondItemChk() {
		if(!secondItemChkBox.isSelected()) {
		secondItemChkBox.click();
		cmn.threadSleepWait(2);
		}
	}

	public void addShippingCharges(String shippingCharges) {
		shippingChargesInput.clear();
		shippingChargesInput.sendKeys(shippingCharges);
	}

	public void inputPONumber(String poNumber) {
		invoicePONumber.sendKeys(poNumber);
		invoicePONumber.sendKeys(Keys.TAB);
		cmn.threadSleepWait(4);
	}

	public void selectHdrLvlChkBox() {
		hdrLvlChkbox.click();
		cmn.threadSleepWait(3);
	}
	
	public void checkStatus(String statusOfInvoice) {
		Assert.assertTrue(statusOfInvoice.equalsIgnoreCase(invoiceStatus.getText()));
	}

	public void clickOnIRLink(String invoiceNumber) {
		withTimeoutOf(15, TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(By.partialLinkText("IR"+invoiceNumber)));
		getDriver().findElement(By.partialLinkText("IR"+invoiceNumber)).click();
	}

	public void changeIRApprover(String irApproverName) {
//		withTimeoutOf(10,TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(actionsDropDownArrow));
		cmn.threadSleepWait(3);
		actionsDropDownArrow.click();
		cmn.threadSleepWait(2);
		checkout.changeToApproverType("serial", irApproverName);
		cmn.threadSleepWait(3);
		changeApproverOkBtn.click();
	}

	public void deleteApprover() {
		deleteApprover.click();
		
		cmn.clickOnOKBtn();
	}

}