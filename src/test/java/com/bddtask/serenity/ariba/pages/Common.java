package com.bddtask.serenity.ariba.pages;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class Common extends PageObject {
	
	// Span List
	@FindBy(xpath = "/html/body/div[5]/div[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td/table/tbody/tr/td[1]/div/div/ul/span[1]/li/span/a")
	private WebElementFacade homeMenuItem;

	@FindBy(xpath = "/html/body/div[5]/div[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td/table/tbody/tr/td[1]/div/div/ul/span[2]/li/span/a")
	private WebElementFacade procurementMenuItem;

	@FindBy(xpath = "/html/body/div[5]/div[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td/table/tbody/tr/td[1]/div/div/ul/span[3]/li/span/a")
	private WebElementFacade invoicingMenuItem;

	@FindBy(xpath = "/html/body/div[5]/div[2]/table/tbody/tr/td[1]/table/tbody/tr[1]/td/table/tbody/tr[3]/td/table/tbody/tr/td[1]/div/div/ul/span[4]/li/span/a")
	private WebElementFacade catalogMenuItem;

	@FindBy(xpath = "//td[@class='leg-font-wght-b portletTitle'][contains(.,'My Documents')]")
	public WebElementFacade myDocumentsTable;

	@FindBy(xpath = "//td[@class='leg-font-wght-b portletTitle'][contains(.,'To Do')]")
	public WebElementFacade toDoTable;

	@FindBy(xpath = "//span[contains(@class,'a-srch-bar-create')]")
	protected WebElementFacade createButton;

	@FindBy(xpath = "//div[@class='a-srch-action-icon']")
	private WebElementFacade searchIcon;

	@FindBy(xpath = "(//div[@class='w-chSearchLink'][contains(.,'Search more')])[3]")
	public WebElementFacade searchMore;

	@FindBy(xpath = "//input[@aria-label='Search for a specific value']")
	public WebElementFacade searchInput;

	@FindBy(xpath = "//button[@title='Search for a specific value in the list']")
	public WebElementFacade searchBtn;

	@FindBy(xpath = "//button[@title='Select this value for the field']")
	public WebElementFacade selectSearchResult;
	
	@FindBy(xpath = "(//button[contains(.,'Select')])[2]")
	public WebElementFacade selectSearchResult2;

	@FindBy(xpath = "(/html/body/div/div/div/div/table/tbody/tr/td/a[contains(.,'Requisition')]")
	public WebElementFacade searchPRresultLink;
	
	@FindBy(xpath = "(//a[@role='menuitem'][contains(.,'Requisition')])[last()]")
	public WebElementFacade requisitionLink;
	
	@FindBy(xpath = "(//a[@role='menuitem'][normalize-space()='Invoice'])[last()]")
	public WebElementFacade invoiceLink;
	
	@FindBy(css = ".w-txt-dsize[aria-labelledby='_vl9nyLabel']")
	public WebElementFacade invoiceSearchInput;
	
	@FindBy(xpath = "(//a[@role='menuitem'][contains(.,'Purchase Order')])[last()]")
	public WebElementFacade purchaseOrderLink;
	
	@FindBy(css = "a[title*='Force an immediate refresh'] div")
	public WebElementFacade refreshButton;

	@FindBy(xpath = "//div[@class='w-back-page-label']")
	private WebElementFacade backButton;
	
	@FindBy(xpath = "(//button[contains(.,'Submit')])[1]")
	private WebElementFacade submitBtn;
	
	@FindBy(xpath = "//button[contains(.,'OK')]")
	private WebElementFacade okBtn;

	public static String PRNumber = "";
	public static String PONumber = "";
	public static String invoiceNumber = "";
	public static String invoiceDateAdder = "";
	
	WebDriverWait driverWait;
	Actions actions;


	public void goToHome() {
		if (homeMenuItem.getAttribute("aria-selected").contains("false")) {
			homeMenuItem.click();
		}
	}

	public void waitUntilVisible(WebElementFacade selector, int secs) {
		withTimeoutOf(Duration.ofSeconds(secs)).waitFor(selector);
	}
	
	public WebElement waitUntilTypable(WebElement selector, int secs) {
		driverWait = new WebDriverWait(getDriver(), secs);
		WebElement waitSelector = driverWait.until(ExpectedConditions.elementToBeClickable(selector));
		return waitSelector;
	}

	public void threadSleepWait(int secs) {
		long time = secs*1000;
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void clickWhenReady(WebElementFacade clickSelector, int secs) {
		waitUntilVisible(clickSelector, secs);
		clickSelector.click();
	}

	public boolean menuItemSelectCheck(WebElementFacade menuItemSelector) {
		boolean flag = false;
		if (menuItemSelector.getAttribute("aria-selected").contains("true")) {
			flag = true;
		}
		return flag;
	}

	public void actionsClick(WebElementFacade selector) {
		waitUntilVisible(selector, 10);
		new Actions(getDriver()).moveToElement(selector).click().perform();		
	}

	public void clickMenuItem(WebElementFacade menuItem) {
		if (!menuItemSelectCheck(menuItem)) {
			menuItem.click();
			threadSleepWait(2);
			if (!menuItemSelectCheck(menuItem)) {
				menuItem.click();
				threadSleepWait(2);
				if (!menuItemSelectCheck(menuItem)) {
					menuItem.click();
				}
			}
		}
	}
	
	public void goToProcurement() {
		clickMenuItem(procurementMenuItem);
	}
	
	public void goToInvoicing() {
		clickMenuItem(invoicingMenuItem);
	}

	public void goToCatalog() {
		if (catalogMenuItem.getAttribute("aria-selected").contains("false")) {
			catalogMenuItem.click();
		}
	}

	public void clickCreateBtn() {
		createButton.click();
	}

	public void searchAndSelect(String searchString) {
		searchMore.click();
		searchInput.sendKeys(searchString);
		searchBtn.click();
		threadSleepWait(2);
		clickWhenReady(selectSearchResult, 3);
	}
	
	public void selectMenuWithoutSearchMore(String searchString) {
		searchInput.sendKeys(searchString);
		searchBtn.click();
		threadSleepWait(2);
		clickWhenReady(selectSearchResult, 3);
	}
	
	public void selectSecondResultMenuWithoutSearchMore(String searchString) {
		searchInput.sendKeys(searchString);
		searchBtn.click();
		threadSleepWait(3);
		clickWhenReady(selectSearchResult2, 3);
	}
	
	public void waitForPSEvent(int durationMins) {
		long durationSecs = durationMins * 60 * 1000;
		try {
			Thread.sleep(durationSecs);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void clickSearchIconBtn() {
		withTimeoutOf(10, TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(searchIcon));
		searchIcon.click();
	}

	public void clickRequisitionSearch() {
		waitFor(requisitionLink);
		requisitionLink.click();
	}
	
	public void clickPOSearch() {
		waitFor(purchaseOrderLink);
		purchaseOrderLink.click();
	}
	
	public String timeStampAdder(String stringThatNeedsTimeStamp) {
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Timestamp(System.currentTimeMillis()));
		return stringThatNeedsTimeStamp+" "+timeStamp;
	}

	public void clickRefreshBtn() {
		refreshButton.click();		
	}

	public void clickBackBtn() {
		withTimeoutOf(15, TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(backButton));
		backButton.click();
	}

	public void clickSubmit() {
		submitBtn.click();
	}

	public void clickInvoiceSearch() {
		threadSleepWait(3);
		withTimeoutOf(15, TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(invoiceLink));
		invoiceLink.click();
	}
	
	public void searchInvoiceByNumber(String invoiceNumber) {
		withTimeoutOf(15, TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(invoiceSearchInput));
		invoiceSearchInput.sendKeys(invoiceNumber);
		searchBtn.click();
	}

	public void clickOnTab(String tabName) {
		threadSleepWait(2);
		getDriver().findElement(By.xpath("//a[contains(.,'"+tabName+"')]")).click();		
	}
	
	public void clickOnOKBtn() {
		withTimeoutOf(15, TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(okBtn));
		okBtn.click();
	}

	public String invoiceDateGenerator() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
		String strDate= formatter.format(date);
		return strDate;
	}
}