package com.bddtask.serenity.ariba.pages;
import java.time.Duration;

import org.junit.Assert;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PurchaseRequestPage extends PageObject {

	Common cmn;
	
	@FindBy(xpath = "//span[@class='a-arp-approvable-state']")
	private WebElementFacade prStatus;
	
	@FindBy(xpath = "//button[@title='Receive this request']")
	private WebElementFacade receiveBtn;
	
	@FindBy(xpath = "//button[@title='Delete this request']")
	private WebElementFacade deleteBtn;
	
	@FindBy(xpath = "//button[@title='Copy to a new request']")
	private WebElementFacade copyBtn;
	
	@FindBy(xpath = "//button[@title='Edit this request']")
	private WebElementFacade editPRBtn;
	
	@FindBy(xpath = "//button[@title='Submit this request']")
	private WebElementFacade submitPRBtn;
	
	@FindBy(xpath = "/html/body/div[5]/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/div/div/div[1]/form/table[1]/tbody/tr[4]/td/div/div[2]/div/table[1]/tbody/tr/td[1]/span/table/tbody/tr[2]/td[3]")
	private WebElementFacade prPagePRTitle;

	@FindBy(xpath="//a[contains(.,'Orders')]")
	private WebElementFacade ordersTab;
	
	@FindBy(xpath="(//span[contains(.,'0000')])[2]")
	private WebElementFacade poID;
	
	public void checkPRStatus(String string1, String string2, String string3) {
		withTimeoutOf(Duration.ofSeconds(10)).waitFor(prStatus);
		String prStatusLowerCase = prStatus.getText().toLowerCase();
		Assert.assertTrue(prStatusLowerCase.equals(string1.toLowerCase())||(prStatusLowerCase.equals(string2.toLowerCase()))||(prStatusLowerCase.equals(string3.toLowerCase())));
	}
	
	public void checkPRStatus(String string1) {
		waitFor(prStatus).isVisible();
		String prStatusLowerCase = prStatus.getText().toLowerCase();
		Assert.assertTrue(prStatusLowerCase.equals(string1.toLowerCase()));
	}
	
	public void checkPRStatus(String string, String string2) {
		waitFor(prStatus).isVisible();
		String prStatusLowerCase = prStatus.getText().toLowerCase();
		Assert.assertTrue(prStatusLowerCase.equals(string.toLowerCase())||(prStatusLowerCase.equals(string2.toLowerCase())));
	}
	
	public boolean checkOrderedStatus() {
		waitFor(prStatus).isVisible();
		String prStatusLowerCase = prStatus.getText().toLowerCase();
		if (prStatusLowerCase.equals("ordered")){
			return true;	
		}else {
		return false;
		}
	}

	public void clickReceive() {
			if (!checkOrderedStatus()) //If Status is not Ordered
			{
				getDriver().navigate().refresh();
			}
			receiveBtn.click();
	}

	public void clickDeleteBtn() {
		deleteBtn.click();
	}

	public void clickCopyBtn() {
		copyBtn.click();		
	}

	public void capturePONumber() {
		cmn.threadSleepWait(5);
		cmn.PONumber = poID.getText();
	}
}