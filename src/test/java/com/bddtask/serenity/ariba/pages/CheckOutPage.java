package com.bddtask.serenity.ariba.pages;
import java.time.Duration;
import java.util.List;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CheckOutPage extends PageObject {

	Common cmn;
	
	@FindBy(xpath = "//label[contains(.,'Title:')]/following::input[1]")
	private WebElementFacade prTitle;
	
	@FindBy(xpath = "//label[contains(.,'Reason')]/following::textarea[1]")
	private WebElementFacade reasonForPurchaseSel;
	
	@FindBy(xpath = "//label[contains(.,'Archibus:')]/following::div[1]")
	private WebElementFacade archibusSelectYes;
	
	@FindBy(xpath = "//label[contains(.,'Archibus:')]/following::div[2]")
	private WebElementFacade archibusSelectNo;
		
	@FindBy(xpath = "//label[contains(.,'Ship')]/following::td[2]/div/div/span/div/div[2]/a/div/div")
	private WebElementFacade shipToSelect;

	@FindBy(xpath = "//label[contains(.,'Delivery')]/following::input[1]")
	private WebElementFacade deliveryInfoInput;
	
	@FindBy(xpath = "//button[contains(.,'Actions')]")
	private WebElementFacade lineActionsBtn;
	
	@FindBy(xpath = "/html/body/div/form/div/div/div/a[2]")
	private WebElementFacade editLineDetails;
	
	@FindBy(xpath = "(//button[contains(.,'Submit')])[1]")
	private WebElementFacade submitPRTop;
	
	@FindBy(xpath = "(//button[contains(.,'Submit')])[2]")
	private WebElementFacade submitPRBottom;
	
	@FindBy(xpath="//html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[1]/td/table/tbody/tr/td[1]/span[1]")
	private WebElementFacade prNumber;
	
	@FindBy(xpath = "//span[contains(.,'Show Approval Flow')]")
	private WebElementFacade showApprovalFlow;
	
	@FindBy(css = "#_abqjdb .w-apv-dropdown-icon")
	private WebElementFacade actionsMenuButton;
	
	@FindBy(partialLinkText = "Add Serial Approver")
	private WebElementFacade addSerialApprover;
	
	@FindBy(className = "w-chInput")
	private WebElementFacade inputSerialApprover;
	
	@FindBy(css = "[title='Done Button']")
	private WebElementFacade doneButton;
	
	@FindBy(css = ".w-btn-primary")
	private WebElementFacade okayButton;
	
	@FindBy(css = ".w-apvPending-cell .w-apv-delete-icon")
	private WebElementFacade pendingDelete;
	
	@FindBy(css = "[title='Delete Approver']")
	private WebElementFacade deleteApproverButton;
	
	public void addPRTitle(String prTitleString) {
		prTitle.sendKeys(prTitleString);
	}
	
	public void inputShipTo(String inputString) {
		shipToSelect.click();
		cmn.searchAndSelect(inputString);
	}

	public void inputDeliveryInfo(String deliveryInfo) {
		deliveryInfoInput.sendKeys(deliveryInfo);			
	}

	public void editLineDetails() {
		lineActionsBtn.click();
		editLineDetails.click();
	}
	
	public void submitPR() {
//		WebElement visiblePRNum = cmn.waitUntilVisible(prNumber, 15);
		cmn.waitUntilVisible(prNumber, 15);
		Common.PRNumber = prNumber.getText().substring(0,prNumber.getText().length()-1);
		cmn.threadSleepWait(5);
		submitPRBottom.click();
		cmn.threadSleepWait(5);
	}

	public void inputReasonForPurchase(String reasonForPurchase) {
	reasonForPurchaseSel.sendKeys(reasonForPurchase);
	}

	public void archibusSelect(String archibusOptionSelection) {
		
		if (archibusOptionSelection.equals("Yes")) {
			archibusSelectYes.click();
		}
		else {
			archibusSelectNo.click();
		}
		
	}
	
	public void showApprovalFlow() {
		showApprovalFlow.click();
		cmn.threadSleepWait(5);
	}

	public void checkIfApproverPresent(String approverGroup) {
		getDriver().findElement(By.linkText(approverGroup));
		cmn.threadSleepWait(3);
	}

	public void changeApprover(String approverName, String typeOfApprover, String deleteApprover) {
		actionsMenuButton.click();
		changeToApproverType(typeOfApprover, approverName);
		deleteApprover(deleteApprover);
	}
	
	public void changeToApproverType(String typeOfApprover, String approverName) {
		if(typeOfApprover.toLowerCase().equals("serial")) {
			addSerialApprover.click();
			withTimeoutOf(Duration.ofSeconds(15)).waitFor(ExpectedConditions.elementToBeClickable(inputSerialApprover));
			inputSerialApprover.sendKeys(approverName);
			inputSerialApprover.sendKeys(Keys.ENTER);
		}
		withTimeoutOf(Duration.ofSeconds(5)).waitFor(doneButton);
		doneButton.click();
		clickOnOk();
	}
	
	public void clickOnOk() {
		withTimeoutOf(Duration.ofSeconds(15)).waitFor(okayButton);
		okayButton.click();
	}
	
	public void deleteApprover(String deleteApprover) {
		if (deleteApprover.equalsIgnoreCase("usyd unibuy desk"))
		{
//			withTimeoutOf(Duration.ofSeconds(5)).waitFor(ExpectedConditions.elementToBeClickable(pendingDelete));
			cmn.threadSleepWait(2);
			pendingDelete.click();
			cmn.threadSleepWait(3);
			deleteApproverButton.click();
			cmn.threadSleepWait(3);	
		}
	}
}