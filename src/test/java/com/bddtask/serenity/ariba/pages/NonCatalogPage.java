package com.bddtask.serenity.ariba.pages;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class NonCatalogPage extends PageObject {

	Common cmn;

	@FindBy(css = "[bh='ROV']:nth-of-type(2)>td:nth-child(3)>textarea")
	private WebElementFacade itemFullDescription;

	@FindBy(css = "[bh='ROV']+tr:nth-of-type(3)>td:nth-child(3)>div>div>span>div>div:nth-child(2)")
	private WebElementFacade commodityCodeDropDown;

	@FindBy(xpath = "/html/body/div/form/div/div/div/a/div")
	private WebElementFacade commodityCodeSearchMore;

	@FindBy(xpath = "//label[contains(.,'Quantity:')]/following::input[1]")
	private WebElementFacade nonCatQty;

	@FindBy(xpath = "/html/body/div/form/div/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div/table/tbody/tr/td/div/table/tbody/tr/td[1]/table/tbody/tr[7]/td[3]/div/div/span/div/div[2]")
	private WebElementFacade uomDropDown;
	
	@FindBy(xpath = "/html/body/div/form/div/div/div/a/div")
	private WebElementFacade uomSearchMore;

	@FindBy(xpath = "//label[contains(.,'Price:')]/following::input[1]")
	private WebElementFacade itemPrice;
	
	@FindBy(xpath = "//label[contains(.,'Price:')]/following::a[1]")
	private WebElementFacade currencyDropDown;
	
	@FindBy(xpath = "//span[contains(.,'Other')]")
	private WebElementFacade currencyDropDownOther;
	
	@FindBy(xpath = "//span[contains(.,'Name')]")
	private WebElementFacade currencyNameSel;

	@FindBy(xpath = "//html/body/div/div/div/div/div/div/div/div/div/div/table/tbody/tr[1]/td/form/table/tbody/tr/td/div/div/div/div[contains(.,'ID')]")
	private WebElementFacade currencyIDSel;
	
	@FindBy(xpath = "//label[contains(.,'Supplier:')]/following::td[2]/div/div/span/div/div[2]/a/div")
	private WebElementFacade supplierDropDown;

	@FindBy(xpath = "(//div[contains(@class,'w-chSearchLink')])[4]")
	private WebElementFacade supplierSearchMore;
	
	@FindBy(xpath = "(//button[contains(.,'Add to Cart')])[2]")
	private WebElementFacade addToCart;
	
	public void enterItemDescription(String itemDescription) {
		withTimeoutOf(Duration.ofSeconds(5)).waitFor(itemFullDescription);
		cmn.threadSleepWait(5);
		itemFullDescription.sendKeys(itemDescription);
	}

	public void enterCommodityCode(String commodityCode) {
		commodityCodeDropDown.click();
		cmn.clickWhenReady(commodityCodeSearchMore, 2);
		cmn.selectMenuWithoutSearchMore(commodityCode);
	}

	public void enterQuantity(String qty) {
		withTimeoutOf(Duration.ofSeconds(10)).waitFor(nonCatQty);
		nonCatQty.clear();
		nonCatQty.sendKeys(qty);
	}

	public void enterUOM(String uom) {
		uomDropDown.click();
		cmn.clickWhenReady(uomSearchMore, 2);
		cmn.selectMenuWithoutSearchMore(uom);
	}
	
	public void enterPrice(String price) {
		itemPrice.clear();
		itemPrice.sendKeys(price);
	}
	
	public void selectCurrency(String currency) {
		currencyDropDown.click();
		currencyDropDownOther.click();
		currencyNameSel.click();
		cmn.threadSleepWait(1);
		cmn.clickWhenReady(currencyIDSel, 3); 
		cmn.selectMenuWithoutSearchMore(currency);	
	}
	
	public void selectSupplier(String supplier) {
		JavascriptExecutor jse = (JavascriptExecutor)getDriver();
		jse.executeScript("window.scrollBy(0,250)", "");
		supplierDropDown.click();
//		cmn.threadSleepWait(5);
		withTimeoutOf(Duration.ofSeconds(5)).waitFor(supplierSearchMore);
		supplierSearchMore.click();
		cmn.selectSecondResultMenuWithoutSearchMore(supplier);		
	}
	
	public void addNCItemToCart() {
		withTimeoutOf(Duration.ofSeconds(4)).waitFor(addToCart);
		addToCart.click();
	}

}
