package com.bddtask.serenity.ariba.pages;

import java.time.Duration;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CatalogPage extends PageObject {

	Common cmn;
	
	@FindBy(xpath = "//img[contains(@height,'40')]")
	private WebElementFacade recentlyViewedItemTile;
	
	@FindBy(xpath = "//input[@class='w-txt w-chInput']")
	private WebElementFacade itemSearchBox;
	
	@FindBy(xpath = "//span[contains(@class,'a-cat-srch-submit-icn')]")
	private WebElementFacade itemSearchBtn;
	
	@FindBy(xpath = "//label[@class='w-chk w-chk-dsize w-chk-disabled']")
	private WebElementFacade searchItemChkBox;
	
	@FindBy(xpath = "//button[contains(.,'Add Non-Catalog Item')]")
	private WebElementFacade nonCatItemBtn;
	
	@FindBy(xpath = "//input[@class='w-txt w-txt-dsize']")
	private WebElementFacade searchPageQty;
	
	@FindBy(xpath = "//button[@tabindex='0'][contains(.,'Add')]")
	private WebElementFacade searchPageAddToCartBtn;
	
	@FindBy(xpath = "//a[contains(.,'black whiteboard markers')]")
	private WebElementFacade searchResultsAuto;
	
	@FindBy(xpath = "//span[@class='cart_nb_bg a-cat-cart-nb-bg']")
	private WebElementFacade cartIcon;
	
	@FindBy(xpath = "//a[contains(.,'Proceed to Checkout')]")
	private WebElementFacade proceedToCheckout;
	
	@FindBy(xpath = "//span[@class='olmParentLabel w-olm-item-lbl'][contains(.,'Supplier')]")
	private WebElementFacade searchBySupplierLabel;
	
	@FindBy(xpath = "//span[@class='olmLeaf w-olm-sub-item-leaf'][contains(.,'ELECTRIC GECKO PTY LIMITED')]")
	private WebElementFacade electricGeckoSupplierSelection;
		
	public void enterSearchItem(String searchString) throws InterruptedException {
//		cmn.waitUntilVisible(itemSearchBox, 4);
		cmn.threadSleepWait(2);
		cmn.waitUntilTypable(itemSearchBox, 4);
		itemSearchBox.clear();
		itemSearchBox.sendKeys(searchString);
		itemSearchBox.sendKeys(Keys.ENTER);
	}
	
	public void clickItemSearchButton() {
		itemSearchBtn.click();
	}
		
	public void clickonRecentlyViewedItemTile() {
		recentlyViewedItemTile.click();
	}
	
	public void enterSearchQty(String qty) throws InterruptedException {
		if(searchItemChkBox.isSelected()==false) {
			searchItemChkBox.click();
		}
		searchPageQty.clear();
		searchPageQty.sendKeys(qty);
	}
	
	public void addToCart() {
		searchPageAddToCartBtn.click();
	}
	
	public void proceedToCheckOut() {
		cartIcon.click();
		proceedToCheckout.click();
	}
	
	public void hoverOverSupplier() {
		searchBySupplierLabel.click();
		electricGeckoSupplierSelection.click();
	}
	
	public void clickNonCatBtn() {
		withTimeoutOf(Duration.ofSeconds(5)).waitFor(nonCatItemBtn);
		nonCatItemBtn.click();
	}

}