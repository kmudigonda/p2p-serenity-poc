package com.bddtask.serenity.ariba.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.findby.By;

public class SplitAccountingPage extends PageObject {
	Common cmn;
	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[3]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade locationDropDown;

	@FindBy(xpath = "(//div[@class='w-chSearchLink'])[12]")
	private WebElementFacade locationSearchMore;

	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[4]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade ccDropDown;

	@FindBy(linkText = "Search more")
	private WebElementFacade SearchMore;

	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[5]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade rcDropDown;

	@FindBy(xpath = "//span[contains(.,'Department')]")
	private WebElementFacade deptDescriptionDropDown;

	@FindBy(xpath = "//div[contains(text(),'Department')]")
	private WebElementFacade departmentSelect;

	@FindBy(xpath = "//div[contains(text(),'Description')]")
	private WebElementFacade descriptionSelect;

	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[6]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade pcDropDown;

	@FindBy(xpath = "(//button[contains(.,'OK')])[1]")
	private WebElementFacade okBtnTop;

	@FindBy(xpath = "(//button[contains(.,'OK')])[2]")
	private WebElementFacade okBtnBottom;

	@FindBy(xpath = "//button[contains(.,'Split Accounting')]")
	private WebElementFacade splitAccountingButton;

	@FindBy(xpath = "//label[contains(.,'By')]/following::span[2]")
	private WebElementFacade splitByDropDown;

	@FindBy(id = "_sto8_d0")
	private WebElementFacade splitAmountSelect;

	@FindBy(id = "_sto8_d1")
	private WebElementFacade splitPercentageSelect;

	@FindBy(id = "_sto8_d2")
	private WebElementFacade splitQtySelect;
	
	@FindBy(xpath = "(//label[contains(.,'Split')])[2]/following::input[1]")
	private WebElementFacade SplitInput1;

	@FindBy(xpath = "(//label[contains(.,'Split')])[4]/following::input[1]")
	private WebElementFacade SplitInput2;
	
	public void clickAccountingdropdown(String AccountingType, String splitNumber) {

		switch (AccountingType.toLowerCase()) {
		case "location":
			clickDropDown("(//label[contains(.,'Location:')])[" + splitNumber + "]/following::div[8]");
			break;
		case "cc":
			clickDropDown("(//label[contains(.,'Classification')])[" + splitNumber + "]/following::div[8]");
			break;
		case "rc":
			clickDropDown("(//label[contains(.,'Responsibility')])[" + splitNumber + "]/following::div[8]");
			break;
		case "pc":
			clickDropDown("(//label[contains(.,'Project')])[" + splitNumber + "]/following::div[8]");
			break;
		}

	}

	private void clickDropDown(String xpath) {
		getDriver().findElement(By.xpath(xpath)).click();
	}

	private void inputLocation(String location) {
		cmn.clickWhenReady(locationSearchMore, 3);
		searchInputSelect(location);
	}
	
	private void inputCC(String ccCode) {
		SearchMore.click();
		searchInputSelect(ccCode);
	}

	private void inputRC(String rcCode) {
		SearchMore.click();
		deptDescriptionDropDown.click();
		cmn.threadSleepWait(2);
		descriptionSelect.click();
		searchInputSelect(rcCode);
	}

	private void inputPC(String pcCode) {
		SearchMore.click();
		searchInputSelect(pcCode);
	}

	private void searchInputSelect(String searchString) {
		cmn.searchInput.sendKeys(searchString);
		cmn.searchBtn.click();
		cmn.threadSleepWait(2);
		cmn.selectSearchResult.click();
		cmn.threadSleepWait(1);
	}

	public void clickOk() {
		okBtnBottom.click();
	}

	public void clickSplitAccounting() {
		splitAccountingButton.click();
	}

	public void clickSplitDropdown() {
		splitByDropDown.click();
	}

	public void selectSplit(String splitBySelection) {
		switch (splitBySelection.toLowerCase()) {
		case "amount":
			splitAmountSelect.click();
			break;
		case "percentage":
			splitPercentageSelect.click();
			break;
		case "quantity":
			splitQtySelect.click();
			break;
		}

	}

	public void addSplit1AccountingValues(String location1, String cC1, String rC1, String pC1, String Split1) {
		
		clickAccountingdropdown("location", "1");
		inputLocation(location1);
		
		clickAccountingdropdown("cc", "1");
		inputCC(cC1);

		clickAccountingdropdown("rc", "1");
		inputRC(rC1);

		clickAccountingdropdown("pc", "1");
		inputPC(pC1);
		
		SplitInput1.clear();
		SplitInput1.sendKeys(Split1);
		
	}

	public void addSplit2AccountingValues(String location2, String cC2, String rC2, String pC2, String Split2) {
		
		clickAccountingdropdown("location", "2");
		inputLocation(location2);
		
		clickAccountingdropdown("cc", "2");
		inputCC(cC2);

		clickAccountingdropdown("rc", "2");
		inputRC(rC2);

		clickAccountingdropdown("pc", "2");
		inputPC(pC2);
		
		SplitInput2.clear();
		SplitInput2.sendKeys(Split2);
	}

}
