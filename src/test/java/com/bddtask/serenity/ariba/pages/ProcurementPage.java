package com.bddtask.serenity.ariba.pages;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

import org.junit.Assert;
import cucumber.runtime.junit.Assertions;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.findby.By;

public class ProcurementPage extends PageObject {
	
	Common cmn;
	
	@FindBy(xpath= "//div[@id='BPR_Body_Inner']/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr/td/div/div/table/tbody/tr/td/div/div/table/tbody/tr/td/div/div/div/div/div/div/div/table/tbody/tr[1]/td/table/tbody/tr[2]/td/a")
	private WebElementFacade createRequisitionLink;
	
	@FindBy(xpath= "//tr[@class=' firstRow tableRow1'][2]")
	private WebElementFacade myDocPRRow;

	@FindBy(xpath= "(//tr[@class=' firstRow tableRow1'])[3]/td[2]/a")
	private WebElementFacade myDocPRNum;
	
	@FindBy(xpath="//button[contains(.,'Approve')]")
	private WebElementFacade approveBtn;
	
	@FindBy(xpath="//button[contains(.,'OK')]")
	private WebElementFacade okBtn;
	
	public void clickRequisitionLink() {
		
		createRequisitionLink.sendKeys(Keys.ENTER);;
	}
	
	private String getPRNum(){
		return myDocPRNum.getText();
	}
	
	public void confirmPRNumber() {
		String capturedPRNumber = Common.PRNumber;
		String myDocPRNumber = getPRNum();
		Assert.assertTrue(capturedPRNumber.equals(myDocPRNumber));
	}
	
	public void clickPR(String prTable) {
		String prNo = "0";
		WebElement prNumberWebElement = null;
		switch (prTable.toLowerCase()) {
		case "mydocs":
			prNo="2";
			prNumberWebElement = getDriver().findElement(By.xpath("((//div[@class='yScroll tableBody'])[1]/table/tbody/tr/following::a[contains(.,'"+Common.PRNumber+"')])"));
			break;
		case "todo":
			prNo="1";
			prNumberWebElement = getDriver().findElement(By.xpath("((//div[@class='yScroll tableBody'])[1]/table/tbody/tr/following::a[contains(.,'"+Common.PRNumber+"')])["+prNo+"]"));
			break;
		}
		cmn.threadSleepWait(3);
		withTimeoutOf(Duration.ofSeconds(10)).waitFor(prNumberWebElement);
		prNumberWebElement.click();
	}

	public void approvePR() {
		approveBtn.click();
		cmn.threadSleepWait(2);
		okBtn.click();
	}
}