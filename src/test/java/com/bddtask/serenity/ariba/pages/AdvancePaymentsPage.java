package com.bddtask.serenity.ariba.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class AdvancePaymentsPage extends PageObject {

	Common cmn;

	@FindBy(xpath = "//span[contains(.,'Create Advance Payment')]")
	private WebElementFacade createAPMTBtn;

	@FindBy(xpath = "//a[contains(.,'Header Level')]")
	private WebElementFacade headerLvl;

	@FindBy(xpath = "//label[contains(.,'Title')]/following::input[1]")
	private WebElementFacade apmtTitle;

	@FindBy(xpath = "//label[contains(.,'Amount')]/following::input[1]")
	private WebElementFacade apmtAmount;

	@FindBy(xpath = "//label[contains(@aria-label,'Select Date')]")
	private WebElementFacade dateSelector;

	@FindBy(xpath = "//td[@class='w-calendar-today w-calendar-selected-day focus']")
	private WebElementFacade highlightedDate;

	public void clickCreateApmt() {
		waitFor(createAPMTBtn);
		createAPMTBtn.click();
	}

	public void selectHdrLvl() {
		waitFor(headerLvl);
		headerLvl.click();
	}

	public void inputTitle(String apmtTitleString) {
		waitFor(apmtTitle);
		apmtTitle.sendKeys(apmtTitleString);
	}

	public void inputAmount(String apmtAmountString) {
		waitFor(apmtAmount);
		apmtAmount.clear();
		apmtAmount.sendKeys(apmtAmountString);
	}

	public void selectTodaysDate() {
		dateSelector.click();
		highlightedDate.click();
	}

}