package com.bddtask.serenity.ariba.pages;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class ReceivePage extends PageObject {

	Common cmn;
	
	@FindBy(xpath = "//button[contains(.,'Accept All')]")
	private WebElementFacade acceptAllBtn;
	
	@FindBy(xpath = "(//button[contains(.,'Submit')])[1]")
	private WebElementFacade receiptSubmitBtn;

	public void acceptAll() {
		acceptAllBtn.click();		
	}
	
	public void receiptSubmit() {
		cmn.threadSleepWait(2);
		receiptSubmitBtn.click();
	}

	
}