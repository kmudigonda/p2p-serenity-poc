package com.bddtask.serenity.ariba.pages;

import org.openqa.selenium.interactions.Actions;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class LineItemDetailPage extends PageObject {
	Common cmn;
	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[3]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade locationDropDown;

	@FindBy(xpath = "/html/body/div[5]/form/div[2]/div[2]/div/a/div")
	private WebElementFacade locationSearchMore;

	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[4]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade ccDropDown;

	@FindBy(linkText = "Search more")
	private WebElementFacade SearchMore;

	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[5]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade rcDropDown;

	@FindBy(xpath = "//span[contains(.,'Department')]")
	private WebElementFacade deptDescriptionDropDown;

	@FindBy(xpath = "//div[contains(text(),'Department')]")
	private WebElementFacade departmentSelect;

	@FindBy(xpath = "//div[contains(text(),'Description')]")
	private WebElementFacade descriptionSelect;

	@FindBy(xpath = "/html/body/div[5]/form/div[2]/table/tbody/tr/td[1]/table/tbody/tr[2]/td/table/tbody/tr/td/div/div/table/tbody/tr[3]/td/div/div/div[1]/div[4]/table[2]/tbody/tr[6]/td[3]/div/div/span/div[1]/div[2]/a/div/div")
	private WebElementFacade pcDropDown;

	@FindBy(xpath = "(//button[contains(.,'OK')])[1]")
	private WebElementFacade okBtnTop;

	@FindBy(xpath = "(//button[contains(.,'OK')])[2]")
	private WebElementFacade okBtnBottom;

	@FindBy(xpath = "//button[contains(.,'Split Accounting')]")
	private WebElementFacade splitAccountingButton;


	public void inputLocation(String location) throws InterruptedException {
		cmn.threadSleepWait(2);
		locationDropDown.click();
		cmn.clickWhenReady(locationSearchMore, 3);
		searchInputSelect(location);
	}

	public void inputCC(String ccCode) throws InterruptedException {
		Thread.sleep(5000L);
		ccDropDown.click();
		Thread.sleep(1000L);
		SearchMore.click();
		searchInputSelect(ccCode);
	}

	public void inputRC(String rcCode) throws InterruptedException {
		rcDropDown.click();
		Thread.sleep(1000L);
		SearchMore.click();
		deptDescriptionDropDown.click();
		Thread.sleep(2000L);
		descriptionSelect.click();
		searchInputSelect(rcCode);
	}

	public void inputPC(String pcCode) throws InterruptedException {
		pcDropDown.click();
		Thread.sleep(1000L);
		SearchMore.click();
		searchInputSelect(pcCode);
	}

	private void searchInputSelect(String searchString) throws InterruptedException {
		cmn.threadSleepWait(3);
		cmn.searchInput.sendKeys(searchString);
		Actions actions = new Actions(getDriver());
		actions.moveToElement(cmn.searchInput).click().perform();
		cmn.searchBtn.click();
		Thread.sleep(2000L);
		cmn.selectSearchResult.click();
		Thread.sleep(2000L);
	}

	public void clickOk() {
//		okBtnBottom.click();
		cmn.clickWhenReady(okBtnBottom, 5);
	}

	public void clickSplitAccounting() {
		splitAccountingButton.click();
	}

}
