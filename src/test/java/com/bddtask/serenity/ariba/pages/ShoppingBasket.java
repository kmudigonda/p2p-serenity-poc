package com.bddtask.serenity.ariba.pages;
import org.junit.Assert;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

public class ShoppingBasket extends PageObject {

	@FindBy(xpath = "//input[contains(@aria-label,'Quantity')]")
	private WebElementFacade quantityTextBox;
	
	@FindBy(xpath = "//button[contains(.,'Add to Cart')]")
	private WebElementFacade addToCartBtn;
	
	public void enterQuantity(String quantityOfItems) {
		quantityTextBox.sendKeys(quantityOfItems);
	}
	
	public void clickAddToCart() {
		addToCartBtn.click();
	}

}