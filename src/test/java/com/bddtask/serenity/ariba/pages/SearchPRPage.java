package com.bddtask.serenity.ariba.pages;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.findby.By;

public class SearchPRPage extends PageObject {

	Common cmn;
	
	@FindBy(xpath = "//label[contains(.,'ID:')]/following::input[1]")
	private WebElementFacade prIDInput;
	
	@FindBy(xpath = "//label[contains(.,'Order ID')]/following::input[1]")
	private WebElementFacade poIDInput;
	
	@FindBy(xpath = "//button[@title='Run this search']")
	private WebElementFacade searchPRBtn;
	
	@FindBy(xpath = "//td[@class='taC']")
	private WebElementFacade searchResultsNumber;
	
	public void searchPRID(String prIDString) {
		prIDInput.clear();
		prIDInput.sendKeys(prIDString);
		searchPRBtn.click();		
	}

	public void searchPOID(String poIDString) {
		poIDInput.clear();
		poIDInput.sendKeys(poIDString);
		searchPRBtn.click();		
	}
	
	public void openSearchedItem(String prID) {
//		WebElement elementToBeClicked = element(By.partialLinkText(prID));
		cmn.threadSleepWait(2);
//		withTimeoutOf(15, TimeUnit.SECONDS).waitFor(ExpectedConditions.elementToBeClickable(getDriver().findElement(By.partialLinkText(prID))));
		getDriver().findElement(By.partialLinkText(prID)).click();		
	}
	
	public void verifySearchResultsNum(String searchResultsNum) {
		cmn.threadSleepWait(2);
		String searchResultsString = "Found "+searchResultsNum+" items";
		assertTrue(searchResultsNumber.getText().equals(searchResultsString));
	}

	public void clickFirstSearchResult() {
		getDriver().findElement(By.xpath("//a[contains(.,'"+Common.PRNumber+"')]")).click();
	}
	
}