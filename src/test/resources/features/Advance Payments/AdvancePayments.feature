Feature: Create an Advance Payment for a Purchase Request
  As a requester
  I want to open Ariba Procurement page
  and create a Purchase Request and Submit
  and I should be able to create an Advance Payment

  @ignore
  Scenario Outline: Create a Catalog Purchase Request, Submit, create an Advance Payment, Full Receive and Invoice
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on Requisition
    When I search by Supplier
    And I search for "<SearchItem>"
    And I enter quantity as "<Quantity>"
    And I click add to cart on search results
    When I proceed to checkout
    And I fill "<ReasonForPurchase>", "<ShipTo>" and "<DeliveryInformation>" fields on Checkout page
    And I check Archibus option as "<ArchibusSelection>"
    And I click on Actions followed by edit line details
    And I enter "<Accounting_Location>", "<ClassificationCode>", "<ResponsibilityCenter>" and "<ProjectCode>"
    And I click OK
    And I click Submit Purchase Request
    When I go to Procurement page
    Then my PR number should be visible
    And I open my PR
    And status of my PR should be Submitted, Ordering or Ordered

    #And I wait for "25" minutes for PO is dispatched
    #And I navigate to Unibuy
    #When I click on the Search icon and select Requisition
    #And I search for the created PR Number and open it
    #And if PR Status is Ordered I click on Receive
    #And I accept all
    #And I Submit Receipt
    Examples: 
      | SearchItem              | Quantity | ReasonForPurchase | ArchibusSelection | ShipTo                 | DeliveryInformation  | Accounting_Location    | ClassificationCode | ResponsibilityCenter | ProjectCode |
      | Blue Whiteboard Markers |       10 | Test Reason       | Yes               | A06, LEVEL 1,Room: 131 | Deliver at Reception | A06, LEVEL 1,Room: 131 |               5027 |                24022 |       00000 |

  @ignore
  Scenario Outline: Create an Advance Payment for a PO and verify GST Treatment in Peoplesoft
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on the Search icon and select "Purchase Order"
    When I search for "PO" Number "<PONumber>"
    And I open the searched "<PONumber>"
    When I click on Create Advance Payment
    And select Header Level
    And I input Advance Payment Title
    And I input Amount as "<Amount>"
    And I select today's date
#		And I select Tax Code as "<TaxCode>"
		And I submit
    Examples: 
      | PONumber   | Amount |
      | 0000244846 |      3 |
