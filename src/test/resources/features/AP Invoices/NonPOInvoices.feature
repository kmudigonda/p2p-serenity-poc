Feature: Create a Non PO Invoices as an Accounts Payable Team member

@ignore
  Scenario Outline: AP Team to create a Non PO Invoice with line level tax, line level shipping charges and shipping tax, approve and reconcile
    Given I login to Ariba as AP Team Member
    When I go to Invoicing page
    And I click on Invoice
    And I select Non-PO Invoice
    And I select Invoice Supplier as "<Supplier>"
    And I input Invoice Number
    And I Add Catalog Item
    And I search for "<SearchItem>"
    And I enter quantity as "<Quantity>"
    And I click add to cart on search results
    And I click on Accounting Info link of the first item
    And I enter Invoice "<Accounting_Location>", "<ClassificationCode>", "<ResponsibilityCenter>" and "<ProjectCode>"
    And I validate and exit
    And I select tax code as "<TaxCode>"
    And I add shipping item
    And I input shipping charges as "<ShippingCharges>"
    And I click on Accounting Info link of the second item
    #And I enter invoice accounting details for shipping item as "<Accounting_Location>", "<ClassificationCode>", "<ResponsibilityCenter>" and "<ProjectCode>"
    And I enter Invoice "<Accounting_Location>", "<ClassificationCode>", "<ResponsibilityCenter>" and "<ProjectCode>" 
    And I validate and exit
    #		And I select first item checkbox
    And I select second item checkbox
    And I select shipping tax code as "<TaxCode>"
    And I submit the invoice
    And I go to Home page
    And I go to Invoicing page
    And I wait for "120" seconds
    And I click on "refresh" button
    And I click on the submitted invoice in to do list

    Examples: 
      | Supplier                   | SearchItem              | Quantity | Accounting_Location    | ClassificationCode | ResponsibilityCenter | ProjectCode | TaxCode          | ShippingCharges |
      | Electric Gecko Pty Limited | Blue Whiteboard Markers |        3 | A06, LEVEL 1,Room: 131 |               5027 |                24022 |       00000 | GST-10-ACQ-OTHER |              15 |
