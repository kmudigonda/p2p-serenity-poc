Feature: Create PO Based Invoices as an Accounts Payable Team member 

@ignore 
Scenario Outline: 
	AP Team to create a PO Based Invoice with line level tax, line level shipping charges and shipping tax, approve and reconcile 
	Given I login to Ariba as AP Team Member 
	When I go to Invoicing page 
	And I click on Invoice 
	And I select PO Invoice 
	And I input Invoice Number 
	And I input PO Number as "<PONumber>" 
	And I select header level checkbox for items 
	And I select tax code as "<TaxCode>" 
	And I add shipping item 
	And I input shipping charges as "<ShippingCharges>" 
	And I select shipping tax code as "<TaxCode>" 
	And I submit the invoice 
	And I go to Home page 
	And I go to Invoicing page 
	And I wait for "20" seconds 
	And I click on "refresh" button 
	And I click on the submitted invoice in to do list 
	
	Examples: 
		| PONumber   | TaxCode          | ShippingCharges |
		| 0000245387 | GST-10-ACQ-OTHER |              15 |
		
Scenario: 
	AP Team to reconcile an Invoice and verify if the Payment is Scheduled and get the Voucher number
	Given I login to Ariba as AP Team Member 
	And I click on the Search icon and select "Invoice"  
	When I input invoice number as "inv- 2019.07.19.15.31.31" 
	And I open the searched "inv- 2019.07.19.15.31.31" 
#	When I click on "Reference" tab
	Then status of the Invoice should be "Reconciling"
#	When I click on IR link
	And I wait for "3" seconds
	And I click on "back" button
	And I wait for "3" seconds
	And I click on "back" button
	And I go to Invoicing page
#	And I click on "refresh" button
	And I wait for "3" seconds
	And I click on IR link
	And I wait for "3" seconds
	When I click on "Approval Flow" tab
	And I delete unwanted approver
	And I change IR Approver to "Kiran Mudigonda"
	And I wait for "3" seconds
	And I delete unwanted approver
	And I wait for "3" seconds