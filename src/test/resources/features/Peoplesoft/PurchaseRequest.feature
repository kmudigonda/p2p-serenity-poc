Feature: Login to Peoplesoft 
	As a requester
  I want to open Ariba Procurement page
  and create a Purchase Request and Submit

#@ignore 
Scenario Outline: Login to Peoplesoft and verify PO status 
	Given I login to Peoplesoft 
	When I go to Navigator 
	And I go to Review Purchase Order Information 
	And I open PO id "<POID>" 
	Then The status of PO should be Approved or Dispatched 
	
	Examples: 
		| POID       |
		| 0000245158 |

@ignore
Scenario: Login to Peoplesoft and run PO Dispatch 
	Given I login to Peoplesoft 
	When I go to Navigator 
	And I go to Dispatch POs 
	And I use run control id as "1" 
#	And I click Run
#	And I click PO Dispatch and Email 
	