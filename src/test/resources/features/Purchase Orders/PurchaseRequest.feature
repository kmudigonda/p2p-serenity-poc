Feature: Create a Catalog Purchase Request
  As a requester
  I want to open Ariba Procurement page
  and create a Purchase Request and Submit

  @ignore
  Scenario Outline: Create a Catalog Purchase Request, Submit, Full Receive and Invoice
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on Requisition
    When I search by Supplier
    And I search for "<SearchItem>"
    And I enter quantity as "<Quantity>"
    And I click add to cart on search results
    When I proceed to checkout
    And I fill "<ReasonForPurchase>", "<ShipTo>" and "<DeliveryInformation>" fields on Checkout page
    And I check Archibus option as "<ArchibusSelection>"
    And I click on Actions followed by edit line details
    And I enter "<Accounting_Location>", "<ClassificationCode>", "<ResponsibilityCenter>" and "<ProjectCode>"
    And I click OK
    And I click Submit Purchase Request
    When I go to Procurement page
    Then my PR number should be visible
    And I open my PR
    And status of my PR should be "Submitted", "Ordering" or "Ordered"

    #And I wait for "25" minutes for PO is dispatched
    #And I navigate to Unibuy
    #When I click on the Search icon and select Requisition
    #And I search for the created PR Number and open it
    #And if PR Status is Ordered I click on Receive
    #And I accept all
    #And I Submit Receipt
    Examples: 
      | SearchItem              | Quantity | ReasonForPurchase | ArchibusSelection | ShipTo                 | DeliveryInformation  | Accounting_Location    | ClassificationCode | ResponsibilityCenter | ProjectCode |
      | Blue Whiteboard Markers |       10 | Test Reason       | Yes               | A06, LEVEL 1,Room: 131 | Deliver at Reception | A06, LEVEL 1,Room: 131 |               5027 |                24022 |       00000 |

  @ignore
  Scenario Outline: Create a Catalog Purchase Request with Split Accounting by Percentage, Submit, Full Receive and Invoice
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on Requisition
    When I search by Supplier
    And I search for "<SearchItem>"
    And I enter quantity as "<Quantity>"
    And I click add to cart on search results
    When I proceed to checkout
    And I fill "<ReasonForPurchase>", "<ShipTo>" and "<DeliveryInformation>" fields on Checkout page
    And I check Archibus option as "<ArchibusSelection>"
    And I click on Actions followed by edit line details
    And I click on Split Accounting
    And I select Split by "Percentage"
    And I enter first split accounting combination as
      | Location1              | CC1  | RC1   | PC1   | SPLIT1 |
      | A06, LEVEL 1,Room: 131 | 5027 | 24022 | 00000 |     35 |
    And I enter second split accounting combination as
      | Location2              | CC2  | RC2   | PC2   | SPLIT2 |
      | A06, LEVEL 1,Room: 131 | 5027 | 24105 | 00000 |     65 |
    And I click OK in Split Accounting
    And I click OK
    And I click Submit Purchase Request
    When I go to Procurement page
    Then my PR number should be visible
    And I open my PR
    And status of my PR should be "Submitted", "Ordering" or "Ordered"

    #And I wait for "25" minutes for PO is dispatched
    #And I navigate to Unibuy
    #When I click on the Search icon and select Requisition
    #And I search for the created PR Number and open it
    #And if PR Status is Ordered I click on Receive
    #And I accept all
    #And I Submit Receipt
    Examples: 
      | SearchItem              | Quantity | ReasonForPurchase | ArchibusSelection | ShipTo                 | DeliveryInformation  |
      | Blue Whiteboard Markers |       10 | Test Reason       | Yes               | A06, LEVEL 1,Room: 131 | Deliver at Reception |

  #### Below Test is to cleanup unwanted PRs in Test Realm ####
  #### Provide all the PR Numbers as a list in the Examples ####
  @ignore
  Scenario Outline: Search for a PR by PR ID and delete it
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on the Search icon and select "Requisition"
    When I search for "PR" Number "<PRNumber>"
    And I open the searched "<PRNumber>"
    And I click on Delete button
    And I search for "PR" Number "<PRNumber>"
    Then "0" results should be found

    Examples: 
      | PRNumber |
      | PR6260   |
      

  @ignore
  Scenario Outline: Search for a PR by PR ID and Copy it
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on the Search icon and select Requisition
    When I search for PR Number "<PRNumber>"
    And I open the searched PR "<PRNumber>"
    And I click on Copy button
      #And I click Submit Purchase Request
      #Then Requisition is submitted successfully
      #Examples:
      | PRNumber |
      | PR2198   |

  @ignore
  Scenario Outline: Search for a PR by PR ID and Receive it Fully
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on the Search icon and select Requisition
    When I search for PR Number "<PRNumber>"
    And I open the searched PR "<PRNumber>"
    And if PR Status is Ordered I click on Receive
    And I accept all
    And I Submit Receipt

    Examples: 
      | PRNumber |
      | PR6244   |
      | PR6245   |
      | PR6246   |
