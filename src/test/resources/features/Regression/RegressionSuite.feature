Feature: Regression Suite covering Non Catalog Items, Catalog Items, POs, Invoices, Advance Payments
  As a requester
  I want to open Ariba Procurement page
  and create a Purchase Request and Submit

#   @ignore
  Scenario Outline: RT_1 Verify Advance Payment is raised for PR Non Catalog Item with Split Accounting, receive, Invoice for the PO, Payment request reconciled, final payment should pay only the remaining amount
    Given I open Ariba as a requester
    And I go to Procurement page
    And I click on Requisition
    And I click on Non-Catalog Item Button
    And I input Item Description
    And I input Commodity Code as "<CommodityCode>"
    And I input Quantity as "<Quantity>" and Unit of Measure as "<UnitofMeasure>"
    And I input item price as "<Price>" and Currency as "<Currency>"
    And I select Supplier as "<Supplier>"
    And I add non catalog item to cart
    When I proceed to checkout
    And I fill "<ReasonForPurchase>", "<ShipTo>" and "<DeliveryInformation>" fields on Checkout page
    And I check Archibus option as "<ArchibusSelection>"
    And I click on Actions followed by edit line details
    And I enter "<Accounting_Location>", "<ClassificationCode>", "<ResponsibilityCenter>" and "<ProjectCode>"
    And I click OK
    And I click on Show Approval Flow
    Then "USYD UniBuy Desk" should be part of the approvers
    And I click Submit Purchase Request
    And I go to Procurement page
    Then my PR number should be visible
    And I open my PR
    And status of my PR should be "Submitted", "Ordering" or "Ordered"
    And I add "Kiran Mudigonda" as "Serial" Approver, delete "USYD UniBuy Desk"
    When I click on "Back" button
    And I click on "Refresh" button
    When I click on the PR in TO DO list
    And I approve PR
    When I open my PR
    Then status of my PR should be Ordering or Ordered
    #When I click on the Search icon and select Requisition
    #And I search for the created PR Number and open it
    #And if PR Status is Ordered I click on Receive
    #And I accept all
    #And I Submit Receipt
    When I click on "Orders" tab
    And I capture the PO Number
    When I click on "Back" button
    When I go to Invoicing page 
	And I click on Invoice 
	And I select PO Invoice 
	And I input Invoice Date and Invoice Number 
	And I input already created PO Number
	And I select header level checkbox for items
	And I select tax code as "<TaxCode>" 
	And I add shipping item 
	And I input shipping charges as "<ShippingCharges>" 
	And I select shipping tax code as "<TaxCode>" 
	And I submit the invoice 
	And I go to Home page 
	And I go to Invoicing page 
	And I wait for "20" seconds 
	And I click on "refresh" button 
	And I click on the submitted invoice in to do list 

    Examples: 
      | CommodityCode                | Quantity | UnitofMeasure | Price | Currency | Supplier                   | ArchibusSelection | ReasonForPurchase | ShipTo                 | DeliveryInformation  | Accounting_Location    | ClassificationCode | ResponsibilityCenter | ProjectCode | TaxCode          | ShippingCharges |
      | Paper Materials and Products |        1 | each          |    10 | AUD      | Electric Gecko Pty Limited | Yes               | Test Reason       | A06, LEVEL 1,Room: 131 | Deliver at Reception | A06, LEVEL 1,Room: 131 |               5027 |                24022 |       00000 | GST-10-ACQ-OTHER |              15 |

 