SET BROWSER=%1
SET SWITCHES=%2
SET BROWSER_BINARIES=%CD%\src\test\resources\drivers
SET PATH=%PATH%;%BROWSER_BINARIES%
mvn clean verify -Dwebdriver.driver=%BROWSER% -Dchrome.switches=%SWITCHES%
start %CD%\target\site\serenity\index.html